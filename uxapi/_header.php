<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Language" content="zh-TW">
        <meta name="description" content="UXAPI">
        <meta name="keywords" content="UXAPI">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta property="og:site_name" content="UXAPI">
        <meta property="og:title" content="UXAPI">
        <meta property="og:type" content="product">
        <meta property="og:description" content="UXAPI">
        <meta property="og:url" content="">
        <meta property="og:image" content="">
        <title>UXAPI</title>
        <link rel="shortcut icon" href="">
        <meta name="viewport" content="width=device-width, user-scalable=yes">

        <link href="c/css/common.css" rel="stylesheet" type="text/css"/>
        <link href="c/css/3rd-party/font-awesome.min.css" rel="stylesheet" type="text/css"/> 
        <!--[if lt IE 9]>
            <link href="c/css/ie8.css" rel="stylesheet" type="text/css"/>
            <script src="c/js/common/html5.js"></script>
            <script src="c/js/common/3rd-party/respond.js"></script>
        <![endif]-->
    </head>
    <body class="main-body">
        <header class="header-nav">
            <a href="index.php" class="logo"><img src="c/img/frame/api_logo.png" alt="UXAPI"/></a>
            <ul class="main-nav">
                <li> 
                    <a href="#">UXAPI</a>
                    <ul class="sec-nav">
                        <li>
                            <a href="authorize.php">授權方式</a>
                            <!-- <ul class="third-nav">
                                <li><a href="#">說明</a></li>
                                <li><a href="#">request參數</a></li>
                                <li><a href="#">範本</a></li>
                                <li><a href="#">respone參數</a></li>
                                <li><a href="#">錯誤碼</a></li>
                            </ul> -->
                        </li>
                        <li><a href="api_prod.php">商品API</a></li>
                        <li><a href="#">訂單API</a></li>
                        <li><a href="#">金流API</a></li>
                        <li><a href="#">物流API</a></li>
                        <li><a href="#">倉庫API</a></li>
                        <li><a href="#">賣場API</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">說明</a>
                    <ul class="sec-nav">
                        <li><a href="news.php">近期更新</a></li>
                        <li><a href="#">申請加入</a></li>
                    </ul>
                </li>
                <li>
                    <a href="api_add.php">選單管理</a>
                </li>
            </ul>
            <div class="login">
                <i class="fa fa-user"></i><span class="logintxt">Username</span><i class="fa fa-caret-down"></i>
                <a href="#" class="login_sub"><i class="fa fa-sign-out"></i><span class="logintxt">登出</span></a>
            </div>
            <div class="search">
                <input type="text" class="search_input" placeholder="search..." size="5"/>
                <a href="#"><i class="fa fa-search"></i></a>
            </div>
        </header>
        
