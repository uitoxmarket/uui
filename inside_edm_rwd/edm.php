<!DOCTYPE html>
<!--[if IE 6]><html class="no-js ie6 lte9 lte8 lte7" lang="zh-tw"><![endif]-->
<!--[if IE 7]><html class="no-js ie7 lte9 lte8 lte7" lang="zh-tw"><![endif]-->
<!--[if IE 8]><html class="no-js ie8 lte9 lte8" lang="zh-tw"><![endif]-->
<!--[if IE 9]><html class="no-js ie9 lte9" lang="zh-tw"><![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"><!--<![endif]-->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="content-language" content="zh-tw">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	
	<meta http-equiv="Cache-control" content="public" max-age="180">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">
	<meta property="og:site_name" content="ASAP 閃電購物網"/>
	<meta property="og:title" content="ASAP 閃電購物網 - 上網閃電購物第一站、台北市5h全台灣24h閃電到貨"/>
    <meta property="og:url" content="http://www.asap.com.tw"/>
    <meta property="og:image" content="assets/c/img/asap-fb.jpg"/>
    <meta property="og:type" content="product" />
	<meta name="description" property="og:description" content="想體驗閃電到貨的網路購物體驗，就上ASAP閃電購物網，台北市5h全台灣24h閃電到貨。智慧型手機、時尚精品、化妝品…數萬件商品讓你看的到就買的到，買的到也能閃電馬上拿的到，買錯閃電退貨不囉唷！每日整點下殺，讓你從此愛上網路購物">
	<meta name="keywords" content="ASAP 閃電購物網 - 上網閃電購物第一站、台北市5h全台灣24h閃電到貨">
	<title>ASAP閃電購物網 - 上網閃電購物第一站、台北市5h全台灣24h閃電到貨</title>
    <link rel="shortcut icon" href="http://www.asap.com.tw/lib/tw_zh-tw/img/asap_favicon.ico">

    <link rel="stylesheet" href="assets/c/css/edm.css">

	<!--[if lt IE 9]>
	    <script src="assets/c/js/3rd-party/html5shiv.js"></script>	    
	    <script src="assets/c/js/3rd-party/selectivizr.js" ></script>
	    <script src="assets/c/js/3rd-party/respond.min.js"></script>
	<![endif]-->
</head>

<body>	

	<div class="edm-container">
		<div class="edm-main-content">
		<!--edm HTML content-->
			
			<!--單傳圖片 sample 圖寬1220px-->
			<img class="edm-main-img" src="http://fakeimg.pl/1220x500/?text=1220xauto" style="display: none;">
			<!--單傳圖片 sample end-->

			<!--html sample-->
			<div style="width:100%; height:auto; background:url(http://img.asap.com.tw/A/edm/2014/02/01/201402A01000001/bg_20140819161309.jpg) center 0 no-repeat;">
				<div style="max-width:1200px; height;auto; margin: 0 auto;">
					<img src="http://img.asap.com.tw/A/edm/2014/02/01/201402A01000001/main_02_20140819161309.jpg" usemap="#Map" style="width:100%; height:auto;">
	                <map name="Map">
	                  <area shape="rect" coords="584,132,797,546" href="http://www.asap.com.tw/item/201406AM260000985">
	                </map>
                </div>
			</div>
			<!--html sample end-->

		<!--edm HTML content END-->
		</div>

		<div class="edm-second-content">
			<div class="edm-block">
				<!--background style is sample-->
				<h3 class="edm-title" style="background-color:#4a62b8; background-image: url(http://img.asap.com.tw/A/edm/2014/02/01/201402A01000001/emma0011_tit_b_20140819161309.jpg)">勁爆單品挑戰底價</h3>				
			</div>
			<div class="edm-block">
				<div class="edm-box-2" style="background-color:#748dea;"><!--background-color is sample-->
					<div class="edm-box-2-left">
						<figure itemscope itemtype="http://data-vocabulary.org/Product">
		                    <a href="#" class="item-pic"><img itemprop="image" src="http://fakeimg.pl/291x616/" alt=""></a>
		                    <figcaption>
			                    <div class="item-descript">
			                        <a itemprop="name" href="#" class="item-name">TOSHIBA Z30 i3-4010U雙核13.3吋日系高質感事務筆電 (金)</a>
			                    </div>
			                    <div class="item-price">
			                    	<p class="discount-price">
				                        <span itemprop="currency" content="TWD" class="dollar">$</span><span itemprop="lowPrice" class="price">299000</span>
			                        </p>
			                    	<del>			                        	
					                    <span itemprop="currency">$</span>
					                    <span itemprop="highPrice">2,6000</span>
					                </del>	                        
			                    </div>
		                    </figcaption>
	                    </figure>
					</div>
					<ul  class="edm-box-2-right">
						<li>
							<figure itemscope itemtype="http://data-vocabulary.org/Product">
			                    <a href="#" class="item-pic"><img itemprop="image" src="http://img.asap.com.tw/A/show/2014/0227/AM0002392/201402AM270002392_053423733.jpg" alt=""></a>
			                    <figcaption>
				                    <div class="item-descript">
				                        <a itemprop="name" href="#" class="item-name">TOSHIBA Z30 i3-4010U雙核13.3吋日系高質感事務筆電 (金)</a>
				                    </div>
				                    <div class="item-price">
				                    	<p class="discount-price">
					                        <span itemprop="currency" content="TWD" class="dollar">$</span><span itemprop="lowPrice" class="price">299000</span>
				                        </p>
				                    	<del>			                        	
						                    <span itemprop="currency">$</span>
						                    <span itemprop="highPrice">2,6000</span>
						                </del>	                        
				                    </div>
			                    </figcaption>
		                    </figure>
						</li>
						<li>
							<figure itemscope itemtype="http://data-vocabulary.org/Product">
			                    <a href="#" class="item-pic"><img itemprop="image" src="http://img.asap.com.tw/A/show/AW000001/2014/0905/AM0000517/201409AM050000517_140990920361796.jpg" alt=""></a>
			                    <figcaption>
				                    <div class="item-descript">
				                        <a itemprop="name" href="#" class="item-name">TOSHIBA Z30 i3-4010U雙核13.3吋日系高質感事務筆電 (金)</a>
				                    </div>
				                    <div class="item-price">
				                    	<p class="discount-price">
					                        <span itemprop="currency" content="TWD" class="dollar">$</span><span itemprop="lowPrice" class="price">29900</span>
				                        </p>
				                    	<del>			                        	
						                    <span itemprop="currency">$</span>
						                    <span itemprop="highPrice">2,600</span>
						                </del>	                        
				                    </div>
			                    </figcaption>
		                    </figure>
						</li>
						<li>
							<figure itemscope itemtype="http://data-vocabulary.org/Product">
			                    <a href="#" class="item-pic"><img itemprop="image" src="http://img.asap.com.tw/A/show/2013/1111/AM0001366/201311AM110001366_369186528.jpg" alt=""></a>
			                    <figcaption>
				                    <div class="item-descript">
				                        <a itemprop="name" href="#" class="item-name">TOSHIBA Z30 i3-4010U雙核13.3吋日系高質感事務筆電 (金)</a>
				                    </div>
				                    <div class="item-price">
				                    	<p class="discount-price">
					                        <span itemprop="currency" content="TWD" class="dollar">$</span><span itemprop="lowPrice" class="price">29900</span>
				                        </p>
				                    	<del>			                        	
						                    <span itemprop="currency">$</span>
						                    <span itemprop="highPrice">2,600</span>
						                </del>	                        
				                    </div>
			                    </figcaption>
		                    </figure>
						</li>
						<li>
							<figure itemscope itemtype="http://data-vocabulary.org/Product">
			                    <a href="#" class="item-pic"><img itemprop="image" src="http://img.asap.com.tw/A/show/2014/0319/AM0000205/201403AM190000205_919440942.jpg" alt=""></a>
			                    <figcaption>
				                    <div class="item-descript">
				                        <a itemprop="name" href="#" class="item-name">TOSHIBA Z30 i3-4010U雙核13.3吋日系高質感事務筆電 (金)</a>
				                    </div>
				                    <div class="item-price">
				                    	<p class="discount-price">
					                        <span itemprop="currency" content="TWD" class="dollar">$</span><span itemprop="lowPrice" class="price">29900</span>
				                        </p>
				                    	<del>			                        	
						                    <span itemprop="currency">$</span>
						                    <span itemprop="highPrice">2,600</span>
						                </del>	                        
				                    </div>
			                    </figcaption>
		                    </figure>
						</li>
						<li>
							<figure itemscope itemtype="http://data-vocabulary.org/Product">
			                    <a href="#" class="item-pic"><img itemprop="image" src="http://img.asap.com.tw/A/show/2013/1128/AM0000111/201311AM280000111_062448852.jpg" alt=""></a>
			                    <figcaption>
				                    <div class="item-descript">
				                        <a itemprop="name" href="#" class="item-name">TOSHIBA Z30 i3-4010U雙核13.3吋日系高質感事務筆電 (金)</a>
				                    </div>
				                    <div class="item-price">
				                    	<p class="discount-price">
					                        <span itemprop="currency" content="TWD" class="dollar">$</span><span itemprop="lowPrice" class="price">29900</span>
				                        </p>
				                    	<del>			                        	
						                    <span itemprop="currency">$</span>
						                    <span itemprop="highPrice">2,600</span>
						                </del>	                        
				                    </div>
			                    </figcaption>
		                    </figure>
						</li>
						<li>
							<figure itemscope itemtype="http://data-vocabulary.org/Product">
			                    <a href="#" class="item-pic"><img itemprop="image" src="http://img.asap.com.tw/A/show/2014/0425/AM0000264/201404AM250000264_021581063.jpg" alt=""></a>
			                    <figcaption>
				                    <div class="item-descript">
				                        <a itemprop="name" href="#" class="item-name">TOSHIBA Z30 i3-4010U雙核13.3吋日系高質感事務筆電 (金)</a>
				                    </div>
				                    <div class="item-price">
				                    	<p class="discount-price">
					                        <span itemprop="currency" content="TWD" class="dollar">$</span><span itemprop="lowPrice" class="price">29900</span>
				                        </p>
				                    	<del>			                        	
						                    <span itemprop="currency">$</span>
						                    <span itemprop="highPrice">2,600</span>
						                </del>	                        
				                    </div>
			                    </figcaption>
		                    </figure>
						</li>
					</ul>
				</div>				
			</div>
			<div class="edm-block">
				<!--background style is sample-->
				<h3 class="edm-title" style="background-color:#4a62b8; background-image: url(http://img.asap.com.tw/A/edm/2014/02/01/201402A01000001/emma0011_tit_b_20140819161309.jpg)">寵愛自己 美容家電全攻略</h3>				
			</div>
			<div class="edm-block">
				<div class="edm-box-3" style="background-color:#748dea;"><!--background-color is sample-->
					<ul>
						<li>
							<figure itemscope itemtype="http://data-vocabulary.org/Product">
			                    <a href="#" class="item-pic"><img itemprop="image" src="http://img.asap.com.tw/A/show/2013/1104/AM0000757/201311AM040000757_609656195.png" alt=""></a>
			                    <figcaption>
				                    <div class="item-descript">
				                        <a itemprop="name" href="#" class="item-name">TOSHIBA Z30 i3-4010U雙核13.3吋日系高質感事務筆電 (金)</a>
				                    </div>
				                    <div class="item-price">
				                    	<p class="discount-price">
					                        <span itemprop="currency" content="TWD" class="dollar">$</span><span itemprop="lowPrice" class="price">29900</span>
				                        </p>
				                    	<del>			                        	
						                    <span itemprop="currency">$</span>
						                    <span itemprop="highPrice">2,600</span>
						                </del>	                        
				                    </div>
			                    </figcaption>
		                    </figure>
						</li>
						<li>
							<figure itemscope itemtype="http://data-vocabulary.org/Product">
			                    <a href="#" class="item-pic"><img itemprop="image" src="http://img.asap.com.tw/A/show/2014/0107/AM0001281/201401AM070001281_272743225.jpg" alt=""></a>
			                    <figcaption>
				                    <div class="item-descript">
				                        <a itemprop="name" href="#" class="item-name">TOSHIBA Z30 i3-4010U雙核13.3吋日系高質感事務筆電 (金)</a>
				                    </div>
				                    <div class="item-price">
				                    	<p class="discount-price">
					                        <span itemprop="currency" content="TWD" class="dollar">$</span><span itemprop="lowPrice" class="price">29900</span>
				                        </p>
				                    	<del>			                        	
						                    <span itemprop="currency">$</span>
						                    <span itemprop="highPrice">2,600</span>
						                </del>	                        
				                    </div>
			                    </figcaption>
		                    </figure>
						</li>
						<li>
							<figure itemscope itemtype="http://data-vocabulary.org/Product">
			                    <a href="#" class="item-pic"><img itemprop="image" src="http://img.asap.com.tw/A/show/2013/1228/AM0000011/201312AM280000011_517074496.jpg" alt=""></a>
			                    <figcaption>
				                    <div class="item-descript">
				                        <a itemprop="name" href="#" class="item-name">TOSHIBA Z30 i3-4010U雙核13.3吋日系高質感事務筆電 (金)</a>
				                    </div>
				                    <div class="item-price">
				                    	<p class="discount-price">
					                        <span itemprop="currency" content="TWD" class="dollar">$</span><span itemprop="lowPrice" class="price">29900</span>
				                        </p>
				                    	<del>			                        	
						                    <span itemprop="currency">$</span>
						                    <span itemprop="highPrice">2,600</span>
						                </del>	                        
				                    </div>
			                    </figcaption>
		                    </figure>
						</li>
						<li>
							<figure itemscope itemtype="http://data-vocabulary.org/Product">
			                    <a href="#" class="item-pic"><img itemprop="image" src="http://img.asap.com.tw/A/show/2014/0310/AM0000706/201403AM100000706_970617903.jpg" alt=""></a>
			                    <figcaption>
				                    <div class="item-descript">
				                        <a itemprop="name" href="#" class="item-name">TOSHIBA Z30 i3-4010U雙核13.3吋日系高質感事務筆電 (金)</a>
				                    </div>
				                    <div class="item-price">
				                    	<p class="discount-price">
					                        <span itemprop="currency" content="TWD" class="dollar">$</span><span itemprop="lowPrice" class="price">29900</span>
				                        </p>
				                    	<del>			                        	
						                    <span itemprop="currency">$</span>
						                    <span itemprop="highPrice">2,600</span>
						                </del>	                        
				                    </div>
			                    </figcaption>
		                    </figure>
						</li>						
					</ul>
				</div>				
			</div>
			<div class="edm-block">
				<!--background style is sample-->
				<h3 class="edm-title" style="background-color:#4a62b8; background-image: url(http://img.asap.com.tw/A/edm/2014/02/01/201402A01000001/emma0011_tit_b_20140819161309.jpg)">夏季尾巴 特賣下殺</h3>				
			</div>
			<div class="edm-block">
				<div class="edm-box-4" style="background-color:#748dea;"><!--background-color is sample-->
					<ul>
						<li>
							<figure itemscope itemtype="http://data-vocabulary.org/Product">
			                    <a href="#" class="item-pic"><img itemprop="image" src="http://img.asap.com.tw/A/show/2014/0708/AM0000142/201407AM080000142_462360046.jpg" alt=""></a>
			                    <figcaption>
				                    <div class="item-descript">
				                        <a itemprop="name" href="#" class="item-name">TOSHIBA Z30 i3-4010U雙核13.3吋日系高質感事務筆電 (金)</a>
				                        <p itemprop="description" class="item-descript-content">英國王牌小家電，過濾加熱一機完成</p>
				                    </div>
				                    <div class="item-tag">搶購</div>
				                    <div class="item-price">
				                    	<p class="discount-price">
					                        <span itemprop="currency" content="TWD" class="dollar">$</span><span itemprop="lowPrice" class="price">29900</span>
				                        </p>
				                    	<del>			                        	
						                    <span itemprop="currency">$</span>
						                    <span itemprop="highPrice">2,600</span>
						                </del>	                        
				                    </div>
			                    </figcaption>
		                    </figure>
						</li>
						<li>
							<figure itemscope itemtype="http://data-vocabulary.org/Product">
			                    <a href="#" class="item-pic"><img itemprop="image" src="http://img.asap.com.tw/A/show/2014/0523/AM0001144/201405AM230001144_103901550.jpg" alt=""></a>
			                    <figcaption>
				                    <div class="item-descript">
				                        <a itemprop="name" href="#" class="item-name">TOSHIBA Z30 i3-4010U雙核13.3吋日系高質感事務筆電 (金)</a>
				                        <p itemprop="description" class="item-descript-content">英國王牌小家電，過濾加熱一機完成</p>
				                    </div>
				                    <div class="item-tag">3期0利率</div>
				                    <div class="item-price">
				                    	<p class="discount-price">
					                        <span itemprop="currency" content="TWD" class="dollar">$</span><span itemprop="lowPrice" class="price">29900</span>
				                        </p>
				                    	<del>			                        	
						                    <span itemprop="currency">$</span>
						                    <span itemprop="highPrice">2,600</span>
						                </del>	                        
				                    </div>
			                    </figcaption>
		                    </figure>
						</li>
						<li>
							<figure itemscope itemtype="http://data-vocabulary.org/Product">
			                    <a href="#" class="item-pic"><img itemprop="image" src="http://img.asap.com.tw/A/show/2014/0706/AM0000016/201407AM060000016_941517967.jpg" alt=""></a>
			                    <figcaption>
				                    <div class="item-descript">
				                        <a itemprop="name" href="#" class="item-name">TOSHIBA Z30 i3-4010U雙核13.3吋日系高質感事務筆電 (金)</a>
				                        <p itemprop="description" class="item-descript-content">英國王牌小家電，過濾加熱一機完成</p>
				                    </div>
				                    <div class="item-tag">搶購</div>
				                    <div class="item-price">
				                    	<p class="discount-price">
					                        <span itemprop="currency" content="TWD" class="dollar">$</span><span itemprop="lowPrice" class="price">29900</span>
				                        </p>
				                    	<del>			                        	
						                    <span itemprop="currency">$</span>
						                    <span itemprop="highPrice">2,600</span>
						                </del>	                        
				                    </div>
			                    </figcaption>
		                    </figure>
						</li>
						<li>
							<figure itemscope itemtype="http://data-vocabulary.org/Product">
			                    <a href="#" class="item-pic"><img itemprop="image" src="http://img.asap.com.tw/A/show/2014/0417/AM0000638/201404AM170000638_867662034.jpg" alt=""></a>
			                    <figcaption>
				                    <div class="item-descript">
				                        <a itemprop="name" href="#" class="item-name">TOSHIBA Z30 i3-4010U雙核13.3吋日系高質感事務筆電 (金)</a>
				                        <p itemprop="description" class="item-descript-content">英國王牌小家電，過濾加熱一機完成</p>
				                    </div>
				                    <div class="item-tag">搶購</div>
				                    <div class="item-price">
				                    	<p class="discount-price">
					                        <span itemprop="currency" content="TWD" class="dollar">$</span><span itemprop="lowPrice" class="price">29900</span>
				                        </p>
				                    	<del>			                        	
						                    <span itemprop="currency">$</span>
						                    <span itemprop="highPrice">2,600</span>
						                </del>	                        
				                    </div>
			                    </figcaption>
		                    </figure>
						</li>

											
					</ul>
				</div>				
			</div>
			<div class="edm-block">
				<!--background style is sample-->
				<h3 class="edm-title" style="background-color:#4a62b8; background-image: url(http://img.asap.com.tw/A/edm/2014/02/01/201402A01000001/emma0011_tit_b_20140819161309.jpg)">夏季尾巴 特賣下殺</h3>				
			</div>
			<div class="edm-block">
				<div class="edm-box-5" style="background-color:#748dea;"><!--background-color is sample-->
					<ul>
						<li>
			                <a href="#"><img src="http://fakeimg.pl/572x334/" alt=""></a>			                    
						</li>
						<li>
							<a href="#"><img src="http://fakeimg.pl/572x334/" alt=""></a>
						</li>			
					</ul>
				</div>				
			</div>
		</div>
		<div class="edm-footer">
			<p>活動網頁的商品、規格、顏色、價位如與銷售網頁不符，請以銷售網頁為準；活動特價商品均限量供應，售完為止！</p>
		</div>
	</div>

	<!-- js -->
	<script src="assets/c/js/jquery.min.js"></script>
	<script src="assets/c/js/3rd-party/jquery.rwdImageMaps.min.js"></script>

	<script>
		$(document).ready(function(){
			//rwdImageMaps 
			$('img[usemap]').rwdImageMaps();	
        });        
	</script>
</body>

</html>
