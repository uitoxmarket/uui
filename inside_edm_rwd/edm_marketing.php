<!DOCTYPE html>
<html class="no-js">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="content-language" content="zh-tw">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	
	<meta http-equiv="Cache-control" content="public" max-age="180">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">
	<meta property="og:site_name" content="ASAP 閃電購物網"/>
	<meta property="og:title" content="ASAP 閃電購物網 - 上網閃電購物第一站、台北市5h全台灣24h閃電到貨"/>
    <meta property="og:url" content="http://www.asap.com.tw"/>
    <meta property="og:image" content="assets/c/img/asap-fb.jpg"/>
    <meta property="og:type" content="product" />
	<meta name="description" property="og:description" content="想體驗閃電到貨的網路購物體驗，就上ASAP閃電購物網，台北市5h全台灣24h閃電到貨。智慧型手機、時尚精品、化妝品…數萬件商品讓你看的到就買的到，買的到也能閃電馬上拿的到，買錯閃電退貨不囉唷！每日整點下殺，讓你從此愛上網路購物">
	<meta name="keywords" content="ASAP 閃電購物網 - 上網閃電購物第一站、台北市5h全台灣24h閃電到貨">
	<title>ASAP閃電購物網 - 上網閃電購物第一站、台北市5h全台灣24h閃電到貨</title>
    <link rel="shortcut icon" href="http://www.asap.com.tw/lib/tw_zh-tw/img/asap_favicon.ico">

    <link rel="stylesheet" href="assets/c/css/edm_marketing.css">

	<!--[if lt IE 9]>
	    <script src="assets/c/js/3rd-party/html5shiv.js"></script>	    
	    <script src="assets/c/js/3rd-party/selectivizr.js" ></script>
	    <script src="assets/c/js/3rd-party/respond.min.js"></script>
	<![endif]-->
</head>

<body>	
	<div class="edm-wrap">
		<div class="edm-content">
			<!-- banner -->
			<?php include "edm_marketing/_edm_banner.php";?>
			<!-- menu -->
			<?php include "edm_marketing/_edm_menu.php";?>

			<div class="edm-act-block">
				<?php include "edm_marketing/_edm_act_block_1.php";?>
				<?php include "edm_marketing/_edm_act_title_1.php";?>
				<?php include "edm_marketing/_edm_act_block_2.php";?>
				<?php include "edm_marketing/_edm_act_title_1.php";?>
				<?php include "edm_marketing/_edm_act_block_3.php";?>
				
			</div>

		</div>
	</div>

	<!-- js -->
	<!--<script src="assets/c/js/jquery.min.js"></script>
	<script src="assets/c/js/3rd-party/jquery.rwdImageMaps.min.js"></script>

	<script>
		$(document).ready(function(){
			//rwdImageMaps 
			$('img[usemap]').rwdImageMaps();	
        });        
	</script>-->
</body>

</html>
