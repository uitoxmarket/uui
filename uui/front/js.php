<?php include "../frame/__data.php";?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> 
	<title>UUI template guideline v0.1</title>
	<link rel="shortcut icon" href="../assets/img/favicon.ico" />
	<link rel="stylesheet" type="text/css" href="../assets/css/frame/frame.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/front/index.css">
	<!--[if lt IE 9]>
        <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
        <script src="../assets/js/3rd-party/respond.js"></script>
    <![endif]-->
</head>
<body>
	<?php include "../frame/_header.php";?>
	<section class="doc-header">
		<div class="container">
			<h1>JS</h1>
			<p class="lead">JavaScript library, offer common usful to develop.</p>
		</div>
	</section>
	<section class="block">
		<div class="container">
			<h1 class="page-header">JS library <span class="date">(更新日期：2014-07-28 16:00)</span></h1>
			<ul>
				<li><a href="http://jquery.com/">jquery-1.9.1.min.js</a></li>
				<li><a href="http://code.google.com/p/ie7-js/">IE9.js</a></li>
				<li><a href="https://github.com/scottjehl/Respond">respond.js</a></li>
				<li><a href="http://www.idangero.us/sliders/swiper/">swiper slide 2.6.1</a></li>
				<li><a href="http://fancybox.net/howto">jquery.fancybox.pack.js 2.1.5</a></li>
				<li><a href="https://github.com/kamens/jQuery-menu-aim">jquery.menu-aim.min.js</a></li>
			</ul>
		</div>
	</section>
	<?php include "../frame/_footer.php";?>
</body>
</html>
