<header class="header">
	<div class="container">
		<a href="index.php" class="logo"><img src="../assets/img/frame/ui_logo.png" alt=""></a>
		<i class="fa fa-align-justify"></i>
		<ul class="nav-menu">
			<li><a href="../store/index.php">關於我們</a></li>
			<li><a href="">服務項目</a>
				<ul class="sub hide">
					<li><a href="">電子商務建置</a></li>
					<li><a href="">金、物流服務</a></li>
					<li><a href="">倉儲物流服務</a></li>
				</ul>
			</li>
			<li><a href="../store/case.php">成功案例</a></li>
			<li><a href="#case">多種版型</a></li>
			<li><a href="../store/faq.php">官網問題</a></li>
			<li><a href="#" class="button large">我要開店</a></li>
		</ul>
	</div>
</header>