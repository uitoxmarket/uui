<header class="header">
	<div class="container">
		<a href="index.php" class="logo"><img src="../assets/img/frame/ui_logo.png" alt=""></a>
		<ul class="nav-menu">
			<li><a href="../front/index.php">Getting Started</a></li>
			<li><a href="../front/css.php">CSS</a></li>
			<li><a href="#">Components</a></li>
			<li><a href="../front/js.php">JavaScript</a></li>
			<li><a href="../front/template.php">Template</a></li>
		</ul>
	</div>
</header>