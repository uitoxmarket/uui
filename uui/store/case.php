<?php include "../frame/__data.php";?>
<!doctype html>
<html lang="zh-TW">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Language" content="zh-TW">
	<meta content="免費官網購物建站提供者 一天讓你擁有電子商務官網。幫你出貨，超世界級的速度。一條龍解決方案，可代操作。連結世界，出貨全球。" name="description">
	<meta content="品牌電商,官網購物,金流,物流,購物車,倉儲服務,開店工具,uitox inside" name="keywords">
	<meta content="品牌電商,官網購物 | uitox inside" property="og:site_name">
	<meta content="品牌電商,官網購物 | uitox inside" property="og:title">
	<meta content="website" property="og:type">
	<meta content="免費官網購物建站提供者,一天讓你擁有電子商務官網,幫您出貨,超世界級的速度。一條龍解決方案,可代操作,連結世界,出貨全球。" property="og:description">
	<meta content="http://www.uitox.com/inside" property="og:url">
	<meta content="http://www.uitox.com/c/web/inside/assets/img/intro/fb.jpg" property="og:image">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> 
	<title>品牌電商,官網購物 | uitox inside</title>
	<link rel="shortcut icon" href="../assets/img/favicon.ico" />
	<link rel="stylesheet" type="text/css" href="../assets/css/frame/frame.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/store/case.css">
	<!--[if lt IE 9]>
        <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
        <script src="../assets/js/3rd-party/respond.js"></script>
    <![endif]-->
</head>
<body>
	<?php include "../frame/_header_store.php";?>
	<header>
		<div class="container">
			<h2>多種版型免費使用</h2>
			<p>Find templates for your ecommerce website</p>
		</div>
	</header>
	<section class="case block">
		<div class="container">
			<a href="#"><img src="../assets/img/case/c1.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c2.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c3.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c4.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c5.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c6.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c7.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c8.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c9.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c10.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c11.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c12.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c13.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c14.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c15.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c16.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c17.jpg" alt=""></a>
			<a href="#"><img src="../assets/img/case/c18.jpg" alt=""></a>

		</div>
	</section>
	<?php include "../frame/_footer.php";?>
	<?php include "../frame/__js_library.php";?>
</body>
</html>
