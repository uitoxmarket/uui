<div class="site-topbar">
    <div class="container">  
        <nav class="nav-user">
            <ul>
                <li class="link-login-out"><span class="account">user@ui..</span><a href="#">登出</a></li>
                <li class="link-login-in"><a href="#">登入</a></li>
                <li class="link-invoices-win"><a href="../module_component/_site_fancybox_all.php #fancybox-invoice-winning" class="fancybox fancybox.ajax">填中獎發票地址</a></li>
                <li class="link-order-list"><a href="order_list.php">查訂單</a></li>
                <li class="link-follow"><a href="follow_list.php">追蹤清單</a></li>
                <li class="link-more">
                    <div class="link-more-icon">
                        <i class="fa fa-caret-down"></i><i class="fa fa-bars"></i>
                    </div>
                    <ul class="link-more-user">
                        <li class="link-more-close"></li>
                        <li class="link-follow2"><a href="follow_list.php">追蹤清單</a></li>
                        <li class="link-bonus"><a href="bonus_list.php">查購物金</a></li>
                        <li class="link-bonus-exchange"><a href="bonus_exchange.php">兌換購物金</a></li>
                        <li class="link-bonus-info"><a href="bonus_info.php">購物金使用說明</a></li>
                        <li class="link-pw"><a href="#">修改密碼</a></li>
                        <li class="link-address"><a href="address_list.php">收貨通訊錄</a></li> 
                        <li class="link-order-edm"><a href="order_edm.php">取消/訂閱電子報</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
<header class="site-header">
    <div class="container">       
        <a href="index.php" class="logo" itemscope itemtype="http://schema.org/LocalBusiness">
            <img src="<?php echo $logo['images'];?>" alt="">
            <h1 itemprop="name">我的帳戶</h1>
        </a>
        <ul class="top-msg">
            <li><a href="#"><span class="top-msg-content">系統公告系統公告系統公告系統公告系統公告系統公告系統公告系統公告系統公告系統公告系統公告系統公告系統公告系統公告系統公告</span><span class="more">..詳</span></a></li>
            <li><a href="#"><span class="top-msg-content">系統公告系統公告系統公告系統公告系統公告</span><span class="more">..詳</span></a></li>
        </ul>
    </div>
</header>