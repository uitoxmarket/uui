<div class="orderlist_001">
    <div class="container">        
        <div class="order-list-block">
        
            <div class="order-list-top">
                <div class="o-list-top-l">
                    <a href="order_detail.php" class="o-top-info">
                        <ul >
                            <li class="order-date">2014/01/25</li>
                            <li class="order-num">訂單編號：<span>201401AP25000241</span></li>
                            <li class="order-price"><span class="currency">NT$</span><span class="o-price">14,780,000</span></li>                            
                        </ul>
                        <i class="fa fa-angle-right o-list-arrow"></i>
                    </a>
                    <span class="pay-status failure">付款失敗</span>
                </div>              
                <div class="o-list-top-r">
                    <a href="order_detail.php">更多訂單資訊</a>
                    <a href="../module_component/_site_fancybox_all.php #fancybox-order-contact" class="fancybox fancybox.ajax o-contact">聯絡客服</a>
                    <a href="../module_component/_site_fancybox_all.php #fancybox-order-invoice" class="fancybox fancybox.ajax">發票</a>
                    <a href="order_refund.php">退訂</a>
                </div>
            </div>

            <div class="order-list">
                <div class="order-list-content">
                    <div class="order-sn">01</div>
                    <h3 class="order-name"><a href="#">雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F</a></h3>                        
                    <ul class="order-list-r">
                        <li class="order-status"><span class="tag-status is-uitox-5h-delivery">北北桃5h 全台24h</span><span class="tag-status is-uitox-24h-delivery">全台24h</span></li>
                        <li class="order-quantity">99件</li>
                        <li class="order-shipping"><a href="../module_component/_site_fancybox_all.php #fancybox-order-shipping" class="fancybox fancybox.ajax">出貨進度：已出貨</a></li>
                    </ul>                 
                </div>

                <div class="order-list-content group">
                    <div class="order-sn"></div>
                    <h3 class="order-name"><a href="#">CLINIQUE 倩碧 超水感新一代水磁場保濕凝膠 75ml x 2入，包含以下商品：</a></h3>                        
                    <ul class="order-list-r">
                        <li class="order-status"></li>
                        <li class="order-quantity"></li>
                        <li class="order-shipping"></li>
                    </ul> 
                    <div class="order-list-add">
                        <div class="order-sn">02</div>
                        <h3 class="order-name"><a href="#">CLINIQUE 倩碧 超水感 x 2入</a></h3>                        
                        <ul class="order-list-r">
                            <li class="order-status"><span class="tag-status is-vendor-delivery">廠商出貨</span><a href="../module_component/_site_fancybox_all.php #fancybox-order-vendor" class="fancybox fancybox.ajax">聯絡廠商</a></li>
                            <li class="order-quantity">99件</li>
                            <li class="order-shipping"><a href="../module_component/_site_fancybox_all.php #fancybox-order-shipping" class="fancybox fancybox.ajax">出貨進度：尚未出貨</a></li>
                        </ul>
                    </div>
                    <div class="order-list-add">
                        <div class="order-sn">03</div>
                        <h3 class="order-name"><a href="#">雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F</a></h3>
                        <ul class="order-list-r">
                            <li class="order-status"><span class="tag-status is-uitox-delivery">uitox出貨</span></li>
                            <li class="order-quantity">99件</li>
                            <li class="order-shipping"><a href="../module_component/_site_fancybox_all.php #fancybox-order-shipping" class="fancybox fancybox.ajax">出貨進度：已出貨</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="order-list-content">
                    <div class="order-sn">04</div>
                    <h3 class="order-name"><a href="#">加購品 - ICARER 復古系列 iPad mini with Retina 雙折站立 磁扣側掀 手工真皮皮套 - 棕</a></h3>                        
                    <ul class="order-list-r">
                        <li class="order-status"><span class="tag-status is-vendor-delivery">廠商出貨</span><a href="../module_component/_site_fancybox_all.php #fancybox-order-vendor" class="fancybox fancybox.ajax">聯絡廠商</a></li>
                        <li class="order-quantity">9件</li>
                        <li class="order-shipping"><a href="../module_component/_site_fancybox_all.php #fancybox-order-shipping" class="fancybox fancybox.ajax">出貨進度：尚未出貨</a></li>
                    </ul>
                </div>
            </div>

        </div>
        
        <div class="order-list-block">        
            <div class="order-list-top">
                <div class="o-list-top-l">
                    <a href="order_detail.php">
                        <ul class="o-top-info">
                            <li class="order-date">2014/01/25</li>
                            <li class="order-num">訂單編號：<span>201401AP25000241</span></li>
                            <li class="order-price"><span class="currency">NT$</span><span class="o-price">14,780,000</span></li>                            
                        </ul>
                        <i class="fa fa-angle-right o-list-arrow"></i>
                    </a>
                    <a href="../module_component/_site_fancybox_all.php #fancybox-confirm-payment" class="fancybox fancybox.ajax pay-status confirm">確認付款</a>
                </div>              
                <div class="o-list-top-r">
                    <a href="order_detail.php">更多訂單資訊</a>
                    <a href="../module_component/_site_fancybox_all.php #fancybox-order-contact" class="fancybox fancybox.ajax o-contact">聯絡客服</a>
                    <a href="../module_component/_site_fancybox_all.php #fancybox-order-invoice" class="fancybox fancybox.ajax">發票</a>
                    <a href="order_refund.php">退訂</a>
                </div>
            </div>
            <div class="order-list">
                <div class="order-list-content">
                    <div class="order-sn">01</div>
                    <h3 class="order-name"><a href="#">雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F</a></h3>                        
                    <ul class="order-list-r">
                        <li class="order-status"><span class="tag-status is-uitox-delivery">uitox出貨</span></li>
                        <li class="order-quantity">99件</li>
                        <li class="order-shipping"><a href="../module_component/_site_fancybox_all.php #fancybox-order-shipping" class="fancybox fancybox.ajax">出貨進度：已出貨</a></li>
                    </ul>                 
                </div>
            </div>            
        </div>

        <div class="order-list-block">        
            <div class="order-list-top">
                <div class="o-list-top-l">
                    <a href="order_detail.php">
                        <ul class="o-top-info">
                            <li class="order-date">2014/01/25</li>
                            <li class="order-num">訂單編號：<span>201401AP25000241</span></li>
                            <li class="order-price"><span class="currency">NT$</span><span class="o-price">14,780,000</span></li>                            
                        </ul>
                        <i class="fa fa-angle-right o-list-arrow"></i>
                    </a>
                    <a href="../module_component/_site_fancybox_all.php #fancybox-confirm-payment-finish" class="fancybox fancybox.ajax pay-status confirm">確認付款</a>
                </div>              
                <div class="o-list-top-r">
                    <a href="order_detail.php">更多訂單資訊</a>
                    <a href="../module_component/_site_fancybox_all.php #fancybox-order-contact" class="fancybox fancybox.ajax o-contact">聯絡客服</a>
                    <a href="../module_component/_site_fancybox_all.php #fancybox-order-invoice" class="fancybox fancybox.ajax">發票</a>
                    <a href="order_refund.php">退訂</a>
                </div>
            </div>
            <div class="order-list">
                <div class="order-list-content">
                    <div class="order-sn">01</div>
                    <h3 class="order-name"><a href="#">雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F</a></h3>                        
                    <ul class="order-list-r">
                        <li class="order-status"><span class="tag-status is-uitox-delivery">uitox出貨</span></li>
                        <li class="order-quantity">99件</li>
                        <li class="order-shipping"><a href="../module_component/_site_fancybox_all.php #fancybox-order-shipping" class="fancybox fancybox.ajax">出貨進度：尚未出貨</a></li>
                    </ul>                 
                </div>
            </div>            
        </div>

        <div class="order-list-block">        
            <div class="order-list-top">
                <div class="o-list-top-l">
                    <a href="order_detail.php">
                        <ul class="o-top-info">
                            <li class="order-date">2014/01/25</li>
                            <li class="order-num">訂單編號：<span>201401AP25000241</span></li>
                            <li class="order-price"><span class="currency">NT$</span><span class="o-price">14,780,000</span></li>                            
                        </ul>
                        <i class="fa fa-angle-right o-list-arrow"></i>
                    </a>
                    <a href="#" class="pay-status now">立即支付</a>
                </div>              
                <div class="o-list-top-r">
                    <a href="order_detail.php">更多訂單資訊</a>
                    <a href="../module_component/_site_fancybox_all.php #fancybox-order-contact" class="fancybox fancybox.ajax o-contact">聯絡客服</a>
                    <a href="../module_component/_site_fancybox_all.php #fancybox-order-invoice" class="fancybox fancybox.ajax">發票</a>
                    <a href="order_refund.php">退訂</a>
                </div>
            </div>
            <div class="order-list">
                <div class="order-list-content">
                    <div class="order-sn">01</div>
                    <h3 class="order-name"><a href="#">雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F</a></h3>                        
                    <ul class="order-list-r">
                        <li class="order-status"><span class="tag-status is-uitox-delivery">uitox出貨</span></li>
                        <li class="order-quantity">99件</li>
                        <li class="order-shipping"><a href="../module_component/_site_fancybox_all.php #fancybox-order-shipping" class="fancybox fancybox.ajax">出貨進度：已出貨</a></li>
                    </ul>                 
                </div>
            </div>            
        </div>

        <div class="order-list-block">        
            <div class="order-list-top">
                <div class="o-list-top-l">
                    <a href="order_detail.php">
                        <ul class="o-top-info">
                            <li class="order-date">2014/01/25</li>
                            <li class="order-num">訂單編號：<span>201401AP25000241</span></li>
                            <li class="order-price"><span class="currency">NT$</span><span class="o-price">14,780,000</span></li>                            
                        </ul>
                        <i class="fa fa-angle-right o-list-arrow"></i>
                    </a>
                    <a href="order_refund_state.php" class="pay-status r-state">退貨/退款</a>
                </div>              
                <div class="o-list-top-r">
                    <a href="order_detail.php">更多訂單資訊</a>
                    <a href="../module_component/_site_fancybox_all.php #fancybox-order-contact" class="fancybox fancybox.ajax o-contact">聯絡客服</a>
                    <a href="../module_component/_site_fancybox_all.php #fancybox-order-invoice" class="fancybox fancybox.ajax">發票</a>
                    <a href="order_refund.php">退訂</a>
                </div>
            </div>
            <div class="order-list">
                <div class="order-list-content">
                    <div class="order-sn">01</div>
                    <h3 class="order-name"><a href="#">雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F</a></h3>                        
                    <ul class="order-list-r">
                        <li class="order-status"><span class="tag-status is-uitox-delivery">uitox出貨</span></li>
                        <li class="order-quantity">99件</li>
                        <li class="order-shipping"><a href="../module_component/_site_fancybox_all.php #fancybox-order-shipping" class="fancybox fancybox.ajax">出貨進度：已出貨</a></li>
                    </ul>                 
                </div>
            </div>            
        </div>



        
        <div class="order-list-block failure">
            <div class="order-list-top">
                <div class="o-list-top-l">                    
                    <ul class="o-top-info">
                        <li class="order-date">2014/01/25</li>
                        <li class="order-num">訂單編號：<span>201401AP25000241</span></li>
                        <li class="order-price"><span class="currency">NT$</span><span class="o-price">2,391,000</span></li>
                    </ul>
                    <i class="fa fa-angle-right o-list-arrow"></i>
                    <span class="pay-status p-failure">訂單失敗</span>
                </div>
                <div class="o-list-top-r">
                    <a href="../module_component/_site_fancybox_all.php #fancybox-order-contact" class="fancybox fancybox.ajax o-contact">聯絡客服</a>
                </div>
            </div>
            <div class="order-list">
                <div class="order-list-content">
                    <div class="order-sn">01</div>
                    <h3 class="order-name"><a href="#">雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F</a></h3>                        
                    <ul class="order-list-r">
                        <li class="order-status"></li>
                        <li class="order-quantity">9件</li>
                        <li class="order-shipping"></li>
                    </ul>                 
                </div>

                <div class="order-list-content group">
                    <div class="order-sn"></div>
                    <h3 class="order-name"><a href="#">CLINIQUE 倩碧 超水感新一代水磁場保濕凝膠 75ml x 2入，包含以下商品：</a></h3>                        
                    <ul class="order-list-r">
                        <li class="order-status"></li>
                        <li class="order-quantity"></li>
                        <li class="order-shipping"></li>
                    </ul> 
                    <div class="order-list-add">
                        <div class="order-sn">02</div>
                        <h3 class="order-name"><a href="#">CLINIQUE 倩碧 超水感 x 2入</a></h3>                        
                        <ul class="order-list-r">
                            <li class="order-status"></li>
                            <li class="order-quantity">99件</li>
                            <li class="order-shipping"></li>
                        </ul>
                    </div>           
                </div>
            </div>    
        </div>


        <div class="order-list-block failure">
            <div class="order-list-top">
                <div class="o-list-top-l">                    
                    <ul class="o-top-info">
                        <li class="order-date">2014/01/25</li>
                        <li class="order-num">訂單編號：<span>201401AP25000241</span></li>
                        <li class="order-price"><span class="currency">NT$</span><span class="o-price">2,391,000</span></li>
                    </ul>
                    <i class="fa fa-angle-right o-list-arrow"></i>
                    <span class="pay-status p-failure">不接受訂單</span>
                </div>
                <div class="o-list-top-r">
                    <a href="../module_component/_site_fancybox_all.php #fancybox-order-contact" class="fancybox fancybox.ajax o-contact">聯絡客服</a>
                </div>
            </div>
            <div class="order-list">
                <div class="order-list-content">
                    <div class="order-sn">01</div>
                    <h3 class="order-name"><a href="#">雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F</a></h3>                        
                    <ul class="order-list-r">
                        <li class="order-status"></li>
                        <li class="order-quantity">9件</li>
                        <li class="order-shipping"></li>
                    </ul>                 
                </div>
            </div>     
        </div>

    </div>
</div>