<div class="order_refund_confirm_001">
    <div class="container">
    <form>        
        <div class="order-list-block">

            <div class="order-list">
                <div class="order-list-top">
                    <h3>退訂商品資料：</h3>         
                </div>
                <a href="order_refund.php" class="back-refund r-top">返回修改</a>

                <div class="o-list-all">
                    <div class="order-list-content">
                        <div class="order-sn">01</div>
                        <h3 class="order-name">雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F</h3>                        
                        <ul class="order-list-r">                        
                            <li class="order-quantity">99件</li>
                            <li class="order-price">NT$19,900,000</li>
                            <li class="order-returned">退貨原因：不符所需-尺寸不合</li>
                        </ul>           
                    </div>

                    <div class="order-list-content group">
                        <div class="order-sn"></div>
                        <h3 class="order-name">CLINIQUE 倩碧 超水感新一代水磁場保濕凝膠 75ml x 2入，包含以下商品：</h3> 
                        <ul class="order-list-r">                        
                            <li class="order-quantity"></li>
                            <li class="order-price">NT$19,900,000</li>
                            <li class="order-returned"></li>
                        </ul>
                        <div class="order-list-add">
                            <div class="order-sn">02</div>
                            <h3 class="order-name">CLINIQUE 倩碧 超水感 x 2入</h3>                        
                            <ul class="order-list-r">                        
                                <li class="order-quantity">99件</li>
                                <li class="order-price"></li>
                                <li class="order-returned">退貨原因：其他-商品已毀損</li>
                            </ul>
                        </div>
                        <div class="order-list-add">
                            <div class="order-sn">03</div>
                            <h3 class="order-name">雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F</h3>                        
                            <ul class="order-list-r">                        
                                <li class="order-quantity">99件</li>
                                <li class="order-price"></li>
                                <li class="order-returned">退貨原因：不符所需-尺寸不合</li>
                            </ul>
                        </div>           
                    </div>

                    <div class="order-list-content">
                        <div class="order-sn">04</div>
                        <h3 class="order-name">加購品 - ICARER 復古系列 iPad mini with Retina 雙折站立 磁扣側掀 手工真皮皮套 - 棕</h3>                        
                        <ul class="order-list-r">                        
                            <li class="order-quantity">99件</li>
                            <li class="order-price">NT$19,900,000</li>
                            <li class="order-returned">退貨原因：其他-商品已毀損</li>
                        </ul>                 
                    </div>
                </div>
                <div class="o-list-more">查看退訂商品清單<i class="fa fa-chevron-down m-arrow"></i></div>

            </div>

                
            <div class="refund-pickup-data">
                <div class="order-list-top">
                    <h3>退貨人資料：</h3>         
                </div>
                <ul>            
                    <li>
                        <label for="input_recipient" class="label-title">退貨人姓名：</label>
                        <input type="text" id="input_recipient" placeholder="收貨人姓名">
                    </li>
                    <li>
                        <label for="input_recipient_mobile" class="label-title">手機：</label>
                        <input type="tel" id="input_recipient_mobile" placeholder="收貨人手機">
                    </li>
                    <li>
                        <label for="input_recipient_phone" class="label-title">市話 (可不填)：</label>
                        <input type="tel" id="input_recipient_phone" placeholder="收貨人市話 (可不填)">
                    </li>
                    <li class="r-address">
                        <label class="label-title">退貨地址：</label>
                        <select name="" id=""><option value="">請選擇縣市</option></select>
                        <select name="" id=""><option value="">請選擇鄉鎮市區</option></select>
                        <input type="text" id="" placeholder="收貨地址">
                    </li>
                </ul>
            </div>

            <div class="refund-data">
                <div class="order-list-top">
                    <h3>退款資料：</h3>         
                </div>
                <ul> 
                    <li>
                        <h5 class="refund-data-title">退款方式：</h5>
                        <span class="refund-data-way r-way">信用卡退款(花旗)</span>
                    </li>
                    <li>
                        <h5 class="refund-data-title">付款方式：</h5>
                        <span class="refund-data-way">信用卡一次付清</span>
                    </li>                    
                </ul>
            </div>

            <div class="refund-data">
                <div class="order-list-top">
                    <h3>退款資料：</h3>         
                </div>
                <ul> 
                    <li>
                        <h5 class="refund-data-title">退款方式：</h5>
                        <span class="refund-data-way r-way">匯款</span>
                    </li>
                    <li>
                        <h5 class="refund-data-title">付款方式：</h5>
                        <span class="refund-data-way">超商取貨</span>
                    </li>
                    <li>
                        <label class="label-title-s">銀行類型：</label>
                        <select name="" id="" class="refund-data-select"><option value="">請選擇</option></select>
                    </li>
                    <li>
                        <label class="label-title-s">銀行名稱：</label>
                        <select name="" id="" class="refund-data-select"><option value="">請選擇</option></select>
                    </li>
                    <li>
                        <label for="input_refund_name" class="label-title-s">戶名：</label>
                        <input type="text" id="input_refund_name" placeholder="退款人帳戶名稱">
                    </li>
                    <li>
                        <label for="input_refund_account" class="label-title-s">帳號：</label>
                        <input type="text" id="input_refund_account" placeholder="退款人銀行帳號">
                    </li>
                </ul>
            </div>

            <div class="refund-data">
                <p class="refund-data-tip">如果您已經拿到貨，請填寫以下退款資料，若未收到貨，則不需填寫</p>
                <div class="order-list-top">
                    <h3>退款資料：</h3>         
                </div>
                <ul> 
                    <li>
                        <h5 class="refund-data-title">退款方式：</h5>
                        <span class="refund-data-way r-way">匯款</span>
                    </li>
                    <li>
                        <h5 class="refund-data-title">付款方式：</h5>
                        <span class="refund-data-way">貨到付款</span>
                    </li>
                    <li>
                        <label class="label-title-s">銀行類型：</label>
                        <select name="" id="" class="refund-data-select"><option value="">請選擇</option></select>
                    </li>
                    <li>
                        <label class="label-title-s">銀行名稱：</label>
                        <select name="" id="" class="refund-data-select"><option value="">請選擇</option></select>
                    </li>
                    <li>
                        <label for="input_refund_name" class="label-title-s">戶名：</label>
                        <input type="text" id="input_refund_name" placeholder="退款人帳戶名稱">
                    </li>
                    <li>
                        <label for="input_refund_account" class="label-title-s">帳號：</label>
                        <input type="text" id="input_refund_account" placeholder="退款人銀行帳號">
                    </li>
                </ul>
            </div>

            <div class="refund-data">
                <div class="order-list-top">
                    <h3>退款資料：</h3>         
                </div>
                <ul> 
                    <li>
                        <h5 class="refund-data-title">退款方式：</h5>
                        <span class="refund-data-way r-way">不需退款</span>
                    </li>
                    <li>
                        <h5 class="refund-data-title">付款方式：</h5>
                        <span class="refund-data-way">貨到付款</span>
                    </li>                    
                </ul>
            </div>
                
            <div class="refund-confirm-check">
                <label>
                    <input type="checkbox" value="" name="">
                    我同意由 uitox 代為處理退貨退款之發票及折讓單相關事宜
                </label>
            </div>
            
            
            <div class="refund-actions">
                <a href="order_list.php" class="button large gray btn-close">回上頁</a>
                <a href="order_refund_finish.php" class="button large light-blue contact-submit">送出</a>
            </div>

        </div>
    </form>
    </div>
</div>