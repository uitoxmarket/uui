<div class="refund_state_001">
    <div class="container">        
        <div class="order-list-block">        
            <div class="order-list-top">
                <div class="o-list-top-r">
                    <ul>                
                        <li class="order-num">訂單編號：201412AP09000123</li>
                        <li class="refund-apply"><a href="#">申請其他商品退訂</a></li>
                    </ul>
                </div>
                <div class="o-list-top-l">
                    <ul>                
                        <li class="refund-date">退訂日期：2015/02/03</li>
                    </ul>
                </div>
                <div class="o-list-top-m">
                    <ul>                
                        <li class="refund-way">預計退款方式及金額：<span class="refund-price">NT$1,178</span>(退款至銀行帳戶)</li>
                        <li class="refund-bonus">退款時歸還購物金：<span class="refund-price">179</span></li>
                    </ul>
                </div>            
            </div>

            <div class="refund-process">
                <div class="refund-process-block">
                    <div class="refund-process-content">                            
                        <ul>
                            <li class="refund-check"><i class="fa fa-check"></i></li>
                            <li class="refund-p-list">申請退件</li>
                            <li class="refund-p-result r-date">02/04 13:35</li>
                        </ul>
                        <ul>
                            <li class="refund-check"><i class="fa fa-check"></i></li>
                            <li class="refund-p-list">物流取件</li>
                            <li class="refund-p-result">已取件</li>
                        </ul>
                        <ul>
                            <li class="refund-check"><i class="fa fa-check"></i></li>
                            <li class="refund-p-list">賣方確認</li>
                            <li class="refund-p-result">已確認</li>
                        </ul>
                        <ul>
                            <li class="refund-check"></li>
                            <li class="refund-p-list">發票 / 折讓單處理完成</li>
                            <li class="refund-p-result">請將發票或折讓單寄回</li>
                        </ul>
                        <ul>
                            <li class="refund-check"><i class="fa fa-check"></i></li>
                            <li class="refund-p-list">退款方式確認</li>
                            <li class="refund-p-result"><a href="../module_component/_site_fancybox_all.php #fancybox-refund-account" class="fancybox fancybox.ajax">輸入退款帳號</a></li>
                        </ul>                            
                    </div>
                    <div class="refund-process-tips">完成後<br>會盡快退款給您</div>
                </div>
                <div class="refund-help">
                    <a href="../module_component/_site_fancybox_all.php #fancybox-order-contact" class="fancybox fancybox.ajax">聯絡客服</a>                    
                </div>
            </div>

            <div class="order-list">
                <h5 class="o-r-title">退訂商品</h5>
                <div class="order-list-content">
                    <div class="order-sn">01</div>
                    <h3 class="order-name">雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F</h3>                        
                    <ul class="order-list-r">                        
                        <li class="order-quantity">99件</li>
                        <li class="order-price">NT$19,900,000</li>
                        <li class="order-returned">退貨原因：不符所需-尺寸不合</li>
                    </ul>           
                </div>

                <div class="order-list-content group">
                    <div class="order-sn"></div>
                    <h3 class="order-name">CLINIQUE 倩碧 超水感新一代水磁場保濕凝膠 75ml x 2入，包含以下商品：</h3> 
                    <ul class="order-list-r">                        
                        <li class="order-quantity"></li>
                        <li class="order-price">NT$19,900,000</li>
                        <li class="order-returned"></li>
                    </ul>
                    <div class="order-list-add">
                        <div class="order-sn">02</div>
                        <h3 class="order-name">CLINIQUE 倩碧 超水感 x 2入</h3>                        
                        <ul class="order-list-r">                        
                            <li class="order-quantity">99件</li>
                            <li class="order-price"></li>
                            <li class="order-returned">退貨原因：其他-商品已毀損</li>
                        </ul>
                    </div>
                    <div class="order-list-add">
                        <div class="order-sn">03</div>
                        <h3 class="order-name">雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F</h3>                        
                        <ul class="order-list-r">                        
                            <li class="order-quantity">99件</li>
                            <li class="order-price"></li>
                            <li class="order-returned">退貨原因：不符所需-尺寸不合</li>
                        </ul>
                    </div>           
                </div>

                <div class="order-list-content">
                    <div class="order-sn">04</div>
                    <h3 class="order-name">加購品 - ICARER 復古系列 iPad mini with Retina 雙折站立 磁扣側掀 手工真皮皮套 - 棕</h3>                        
                    <ul class="order-list-r">                        
                        <li class="order-quantity">99件</li>
                        <li class="order-price">NT$19,900,000</li>
                        <li class="order-returned">退貨原因：其他-商品已毀損</li>
                    </ul>                 
                </div>                                
            </div>

            <div class="refund-info">
                <h5 class="o-r-title">退貨說明</h5>
                <p>
                    將有宅配公司與您聯繫，我們會盡力於3個工作天內前往取貨，請保持電話暢通，不要拒接不明來電顯示。<br>
                    請將退貨商品完整包裝，連同附件贈品一同退回， 並於外箱貼上退貨寄送單，或將單號抄寫在uitox外箱上。<br>
若無uitox外箱，請您在商品原廠外盒之外，再以其它適當的包裝盒進行包裝。<br>
[切勿直接寫在非uitox外箱，以免影響退貨權益，謝謝您的合作]
                </p>
            </div>
            
            <div class="refund-actions">
                <a href="order_list.php" class="button large light-blue btn-close">補印退貨寄送單</a>
            </div>

            <div class="refund-info">
                <div class="refund-form-content">
                    <h3>退貨寄送單</h3> 
                    <h2>宅配取貨單號：9273893822</h2>
                    <h4>訂單編號：201502AP03001205</h4>
                    <ul class="r-form-data">
                        <li class="r-form-title">收件人</li>
                        <li class="r-form-detail">
                            <dl>
                                <dt>姓名：</dt>
                                <dd>新加坡商優達斯國際有限公司台灣辦事處倉管營運退貨組</dd>
                            </dl>
                            <dl>
                                <dt>地址：</dt>
                                <dd>uitox倉庫(00156)</dd>
                            </dl>
                        </li>
                    </ul>
                    <ul class="r-form-data">
                        <li class="r-form-title">寄件人</li>
                        <li class="r-form-detail">
                            <dl>
                                <dt>姓名：</dt>
                                <dd>王曉明</dd>
                            </dl>
                            <dl>
                                <dt>地址：</dt>
                                <dd>台北市信義區基隆路一段163號9樓</dd>
                            </dl>
                        </li>
                    </ul>
                </div>                                            
            </div>

            <div class="refund-info">
                <div class="refund-form-content">
                    <h3>退貨寄送單</h3>     
                    <h2>訂單編號：201502AP03001205</h2>
                    <ul class="r-form-data">
                        <li class="r-form-title">收件人</li>
                        <li class="r-form-detail">
                            <dl>
                                <dt>姓名：</dt>
                                <dd>新加坡商優達斯國際有限公司台灣辦事處倉管營運退貨組</dd>
                            </dl>
                            <dl>
                                <dt>地址：</dt>
                                <dd>uitox倉庫(00156)</dd>
                            </dl>
                        </li>
                    </ul>
                    <ul class="r-form-data">
                        <li class="r-form-title">寄件人</li>
                        <li class="r-form-detail">
                            <dl>
                                <dt>姓名：</dt>
                                <dd>王曉明</dd>
                            </dl>
                            <dl>
                                <dt>地址：</dt>
                                <dd>台北市信義區基隆路一段163號9樓</dd>
                            </dl>
                        </li>
                    </ul>
                </div>                                            
            </div>

            <div class="refund-info">
                <h5 class="o-r-title">取件時間</h5>
                <p>
                    假設 D日18:00 前 辦理退訂，預計會在 D+ 2 日 前往取貨。<br>
                    假設 D日18:00 後 辦理退訂，預計會在 D+ 3 日 前往取貨。
                </p>
                <p>
                    例如：<br>
                    15日下午5:00 辦理退訂，預計 17日 前往取貨。<br>
                    15日下午6:30 辦理退訂，預計 18日 前往取貨。
                </p>
                <p>
                    ※ 取貨前宅配公司可能與您聯繫，請保持電話暢通，不要拒接不明來電顯示。<br>
                    ※ 實際派車取件時間，可能受物流商派車狀況影響，如有問題，請撥打客服專線：02-2748-1531。
                </p>                               
            </div>

            <div class="refund-info">
                <h5 class="o-r-title">發票處理</h5>
                <p>
                    <a href="order_refund_debitnote.php" target="_blank">列印折讓單</a>
                    <a href="order_refund_debitnote.php" target="_blank">列印折讓單2</a><br>
                    將發票或列印折讓單放入箱中隨貨送回。<br>
                    請注意，折讓單需在一、二聯蓋上公司發票章(或大小章)。<br>
                    若宅配公司已向您取貨，請將發票或折讓單寄回：台北市信義區基隆路一段163號18樓 客服中心收。
                </p>                                                 
            </div>

            <div class="refund-info">
                <h5 class="o-r-title">發票處理</h5>
                <p>
                    uitox代為處理退貨退款折讓單(發票），您不需處理。
                </p>                                                 
            </div>
                 
        </div>

    </div>
</div>