<div class="order_refund_001">
    <div class="container">        
        <div class="order-list-block">        
            <div class="order-list-top">
                <h3>請勾選欲<span>退訂</span>商品：</h3>         
            </div>
            <form>
            <div class="order-list">
                <div class="order-list-content top">
                    <div class="order-check-all"><input type="checkbox" id="" name="" value="">全選</div>                                              
                </div>

                <div class="order-list-content">
                    <label>
                        <div class="order-check"><input type="checkbox" id="" name="" value=""></div>
                        <div class="order-sn">01</div>
                        <h3 class="order-name">
                                雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F
                                <div class="order-status"><span class="tag-status is-vendor-delivery">廠商出貨</span></div>
                        </h3>
                    </label>                   
                    <ul class="order-list-r">                                                
                        <li class="order-price">NT$19,900,000</li>
                        <li class="order-quantity">
                            <select name="" id="">
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                            </select>
                        </li>
                    </ul>
                    <div class="order-list-rr">      
                        <select name="" id=""  class="refund-reason">
                            <option value="">退訂原因</option>
                            <option value="">不符所需</option>
                            <option value="">商品缺件</option>
                            <option value="">規格不合</option>
                            <option value="">不想等太久</option>
                            <option value="">寄錯商品</option>
                            <option value="">商品瑕疵</option>
                            <option value="">其他</option>    
                        </select>
                        <input type="text" placeholder="其他退訂原因" value="" id="" class="refund-reason-other">
                        <a href="javasripy:void(0)" class="refund-reason-same">以下全同</a>
                    </div>                    
                </div>

                <div class="order-list-content group">
                    <label>
                        <div class="order-check"><input type="checkbox" id="" name="" value=""></div>
                        <div class="order-sn"></div>
                        <h3 class="order-name">CLINIQUE 倩碧 超水感新一代水磁場保濕凝膠 75ml x 2入，包含以下商品：</h3>
                    </label>                     
                    <ul class="order-list-r">                       
                        <li class="order-price">NT$19,900,000</li>
                        <li class="order-quantity">
                            <select name="" id="">
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                            </select>
                        </li>
                    </ul>
                    <div class="order-list-rr">      
                        <select name="" id=""  class="refund-reason">
                            <option value="">退訂原因</option>
                            <option value="">不符所需</option>
                            <option value="">商品缺件</option>
                            <option value="">規格不合</option>
                            <option value="">不想等太久</option>
                            <option value="">寄錯商品</option>
                            <option value="">商品瑕疵</option>
                            <option value="">其他</option>    
                        </select>
                        <input type="text" placeholder="其他退訂原因" value="" id="" class="refund-reason-other">
                        <a href="javasripy:void(0)" class="refund-reason-same">同上</a>
                    </div>
                    <div class="order-list-add">
                        <div class="order-check"></div>
                        <div class="order-sn">02</div>
                        <h3 class="order-name">
                            CLINIQUE 倩碧 超水感 x 2入
                            <div class="order-status"><span class="tag-status is-uitox-delivery">uitox出貨</span></div>
                        </h3>
                    </div>
                    <div class="order-list-add">
                        <div class="order-check"></div>
                        <div class="order-sn">03</div>
                        <h3 class="order-name">
                            雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F
                            <div class="order-status"><span class="tag-status is-vendor-delivery">廠商出貨</span></div>
                        </h3>                        
                    </div>           
                </div>

                <div class="order-list-content">
                    <label>
                        <div class="order-check"><input type="checkbox" id="" name="" value=""></div>
                        <div class="order-sn">04</div>
                        <h3 class="order-name">
                            雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F
                            <div class="order-status"><span class="tag-status is-uitox-delivery">uitox出貨</span></div>
                        </h3>
                    </label>                 
                    <ul class="order-list-r">                                                
                        <li class="order-price">NT$19,900,000</li>
                        <li class="order-quantity">
                            <select name="" id="">
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                            </select>
                        </li>
                    </ul>
                    <div class="order-list-rr">      
                        <select name="" id=""  class="refund-reason">
                            <option value="">退訂原因</option>
                            <option value="">不符所需</option>
                            <option value="">商品缺件</option>
                            <option value="">規格不合</option>
                            <option value="">不想等太久</option>
                            <option value="">寄錯商品</option>
                            <option value="">商品瑕疵</option>
                            <option value="">其他</option>    
                        </select>
                        <input type="text" placeholder="其他退訂原因" value="" id="" class="refund-reason-other">
                        <a href="javasripy:void(0)" class="refund-reason-same">同上</a>
                    </div>                    
                </div>

                <div class="order-list-content">
                    <label>
                        <div class="order-check"></div>
                        <div class="order-sn">05</div>
                        <h3 class="order-name">
                            雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F
                            <div class="order-status"><span class="tag-status is-imported">日本直送</span></div>
                        </h3>
                    </label>                
                    <ul class="order-list-r">                                                
                        <li class="order-price">NT$19,900,000</li>
                        <li class="order-quantity"></li>
                    </ul>
                    <div class="order-list-rr"></div>                    
                </div>
            </div>

            <p class="refund-tip">海外直送商品，一般情形下，本公司不接受隨意取消訂單或任意退貨；若因商品本身瑕疵問題，請與 <a href="../module_component/_site_fancybox_all.php #fancybox-order-contact" class="fancybox fancybox.ajax">本公司客服聯絡</a>。</p>
            
            <div class="refund-actions">
                <a href="order_list.php" class="button large gray btn-close">回上頁</a>
                <a href="order_refund_confirm.php" class="button large light-blue contact-submit">送出</a>
            </div>

            </form>
        </div>
    </div>
</div>