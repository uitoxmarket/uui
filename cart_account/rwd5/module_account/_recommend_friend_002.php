        <div class="l-container re-tiny">
            <!-- id management -->
            <h1 class="t-card-title">推薦好朋友</h1>
            <div class="md-account l-card">    
                <dl class="re-list">
                    <dt>推薦人姓名：</dt>
                    <!-- recommended name -->
                    <dd class="form-item captcha">
                        <span class="md-input-item">
                            <input type="name" tabindex="6" class="input-id input-text" id="input_name" name="account" size="29" value="nancy" data-empty="請輸入推薦人姓名!" placeholder="推薦人姓名">
                            <label for="input_name" class="place-label"></label>
                        </span>
                    </dd>
                    <!-- friend name -->
                    <dt>好朋友：</dt>
                    <dd class="form-item clearfix" id="friend_list">
                        <div class="form-block">
                            <span class="number-icon">1</span>
                            <span class="name">好朋友姓名</span>
                            <span class="md-input-item">
                                <input type="name" tabindex="6" class="input-id input-text" id="input_name" name="account" size="16" value="南西" data-empty="請輸入好朋友姓名!" placeholder="好朋友姓名">
                                <label for="input_name" class="place-label"></label>
                            </span>
                            <span class="email">好朋友email</span>
                            <span class="md-input-item">                                
                                <input type="email" tabindex="16" class="input-id input-text" id="input_add_email" name="account" size="29" value="nancy@uitox.com" data-error="輸入格式有誤!" data-empty="請輸入好朋友的 E-mail!" placeholder="好朋友的 E-mail">
                                <label for="input_add_email" class="place-label"></label>
                            </span>
                        </div>
                        <div class="form-block">
                            <span class="number-icon">2</span>
                            <span class="name">好朋友姓名</span>
                            <span class="md-input-item">
                                <input type="name" tabindex="6" class="input-id input-text" id="input_name" name="account" size="16" value="nancy" data-empty="請輸入好朋友姓名!" placeholder="好朋友姓名">
                                <label for="input_name" class="place-label"></label>
                            </span>
                            <span class="email">好朋友email</span>
                            <span class="md-input-item">
                                <input type="email" tabindex="16" class="input-id input-text is-error" id="input_add_email" name="account" size="29" value="nancy@uitox.com" data-error="輸入格式有誤!" data-empty="請輸入好朋友的 E-mail!" placeholder="好朋友的 E-mail">
                                <label for="input_add_email" class="place-label"></label>
                            </span>
                        </div>
                        <div class="form-block">
                            <span class="number-icon">3</span>
                            <span class="name">好朋友姓名</span>
                            <span class="md-input-item">
                                <input type="name" tabindex="6" class="input-id input-text" id="input_name" name="account" size="16" value="阿信" data-empty="請輸入好朋友姓名!" placeholder="好朋友姓名">
                                <label for="input_name" class="place-label"></label>
                            </span>
                            <span class="email">好朋友email</span>
                            <span class="md-input-item">
                                <input type="email" tabindex="16" class="input-id input-text" id="input_add_email" name="account" size="29" data-error="輸入格式有誤!" data-empty="請輸入好朋友的 E-mail!" placeholder="好朋友的 E-mail">
                                <label for="input_add_email" class="place-label"></label>
                            </span>
                        </div>
                        <div class="form-block">
                            <span class="number-icon">4</span>
                            <span class="name">好朋友姓名</span>
                            <span class="md-input-item">
                                <input type="name" tabindex="6" class="input-id input-text" id="input_name" name="account" size="16" value="阿信" data-empty="請輸入好朋友姓名!" placeholder="好朋友姓名">
                                <label for="input_name" class="place-label"></label>
                            </span>
                            <span class="email">好朋友email</span>
                            <span class="md-input-item">
                                <input type="email" tabindex="16" class="input-id input-text" id="input_add_email" name="account" size="29" data-error="輸入格式有誤!" data-empty="請輸入好朋友的 E-mail!" placeholder="好朋友的 E-mail">
                                <label for="input_add_email" class="place-label"></label>
                            </span>
                        </div>
                        <div class="form-block">
                            <span class="number-icon">5</span>
                            <span class="name">好朋友姓名</span>
                            <span class="md-input-item">
                                <input type="name" tabindex="6" class="input-id input-text" id="input_name" name="account" size="16" value="阿信" data-empty="請輸入好朋友姓名!" placeholder="好朋友姓名">
                                <label for="input_name" class="place-label"></label>
                            </span>
                            <span class="email">好朋友email</span>
                            <span class="md-input-item">
                                <input type="email" tabindex="16" class="input-id input-text" id="input_add_email" name="account" size="29" data-error="輸入格式有誤!" data-empty="請輸入好朋友的 E-mail!" placeholder="好朋友的 E-mail">
                                <label for="input_add_email" class="place-label"></label>
                            </span>
                        </div>
                    </dd>
                    <!--<dd class="add-form-block">
                        <button id="invite_plus" class="btn-add-address go-popup"> + 新增好朋友資訊</button>
                    </dd>-->
                </dl>
                <div class="re-action">
                    <button href="../module_component/_site_fancybox_all.php #fancybox-recommend-friend" class="btn-submit fancybox fancybox.ajax">送出推薦邀請</button>
                    <button class="btn-cancel" tabindex="20">重新填寫</button>
                </div>
            </div>
        </div>