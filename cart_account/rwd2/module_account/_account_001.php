<div class="account_001">
    <div class="container">        
        <nav class="account-menu">            
            <a href="order_list.php"><i class="fa fa-file-text"></i><p>查訂單(聯絡客服)</p></a>                    
            <a href="bonus_list.php"><i class="fa fa-money"></i><p>查購物金</p></a>
            <a href="bonus_exchange.php"><i class="fa fa-money"></i><p>兌換購物金</p></a>
            <a href="bonus_info.php"><i class="fa fa-info-circle"></i><p>購物金使用說明</p></a>
            <a href="#"><i class="fa fa-key"></i><p>修改密碼</p></a>           
            <a href="address_list.php"><i class="fa fa-truck"></i><p>收貨通訊錄</p></a>            
            <a href="order_edm.php"><i class="fa fa-newspaper-o"></i><p>取消/訂閱電子報</p></a>
            <a href="#"><i class="fa fa-th-list"></i><p>追蹤清單</p></a>
            <a href="#"><i class="fa fa-pencil-square-o "></i><p>填中獎發票地址</p></a>    
        </nav>
    </div>
</div>