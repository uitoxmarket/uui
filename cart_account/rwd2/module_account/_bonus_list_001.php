<div class="bonus_list_001">
    <div class="container">
        <div class="account-title">查詢購物金<a class="bonus-info" href="bonus_info.php">使用說明</a></div>
        <a href="bonus_exchange.php" class="bonus-exchange">兌換購物金</a>

        <div class="bonus-list-block">
            
            <div class="bonus-count-block">
                <ul class="bonus-count">
                    <li>目前可用點數<span>200</span></li>
                    <li>待生效點數<span>100</span></li>
                </ul>
                <div class="exp-date-info">購物金有效期限60天</div>
            </div>

            <div class="bonus-list">
                <div class="bonus-list-content top">
                    <div class="bonus-num">訂單編號</div>
                    <h3 class="bonus-source">來源</h3>                        
                    <ul class="bonus-list-r">
                        <li class="bonus-quantity"><span>可用</span></span>點數</span></li>                                                               
                        <li class="bonus-exp-date">有效期限</li>
                        <li class="bonus-quantity2"><span>待生效</span><span>點數</span></li>
                        <li class="bonus-effec-date">生效日期</li>                                 
                    </ul>                                         
                </div>
                
                <div class="bonus-list-content">
                    <div class="bonus-num">201412AP09000123</div>
                    <h3 class="bonus-source">中國信託ATM－百元酷碰專區活動(6月)</h3>                        
                    <ul class="bonus-list-r">
                        <li class="bonus-quantity">0</li>                                                               
                        <li class="bonus-exp-date">2015/07/25 23:59</li>
                        <li class="bonus-quantity2">100</li>
                        <li class="bonus-effec-date">2015/06/25</li>                                 
                    </ul> 
                    <div class="bonus-more"><span>明細</span><i class="fa fa-caret-right b-arrow"></i></div>

                    
                    <div class="c"></div>


                    <div class="bonus-list-detail">
                        <ul class="bonus-list-detail-content top">
                            <li class="bonus-detail-date">日期</li>
                            <li class="bonus-detail-num">訂單編號</li>
                            <li class="bonus-detail-quantity">點數</li> 
                            <li class="bonus-detail-reason">原因</li> 
                        </ul>

                        <ul class="bonus-list-detail-content">
                            <li class="bonus-detail-date">06/25</li>
                            <li class="bonus-detail-num">201412AP09000123</li>
                            <li class="bonus-detail-quantity">+100</li> 
                            <li class="bonus-detail-reason"><span class="reason-txt">原因：</span>中國信託ATM－百元酷碰專區活動(6月)</li> 
                        </ul>  
                        <ul class="bonus-list-detail-content">
                            <li class="bonus-detail-date">06/25</li>
                            <li class="bonus-detail-num">-</li>
                            <li class="bonus-detail-quantity">+100</li> 
                            <li class="bonus-detail-reason"><span class="reason-txt">原因：</span>中國信託ATM－百元酷碰專區活動(6月)</li> 
                        </ul>  
                    </div>
                </div>
                
                <div class="bonus-list-content">
                    <div class="bonus-num">201412AP09000123</div>
                    <h3 class="bonus-source">中國信託ATM－百元酷碰專區活動(6月)</h3>                        
                    <ul class="bonus-list-r">
                        <li class="bonus-quantity">0</li>                                                               
                        <li class="bonus-exp-date">2015/07/25 23:59</li>
                        <li class="bonus-quantity2">100</li>
                        <li class="bonus-effec-date">2015/06/25</li>                                 
                    </ul> 
                    <div class="bonus-more"><span>明細</span><i class="fa fa-caret-right b-arrow"></i></div>

                    
                    <div class="c"></div>


                    <div class="bonus-list-detail">
                        <ul class="bonus-list-detail-content top">
                            <li class="bonus-detail-date">日期</li>
                            <li class="bonus-detail-num">訂單編號</li>
                            <li class="bonus-detail-quantity">點數</li> 
                            <li class="bonus-detail-reason">原因</li> 
                        </ul>

                        <ul class="bonus-list-detail-content">
                            <li class="bonus-detail-date">06/25</li>
                            <li class="bonus-detail-num">201412AP09000123</li>
                            <li class="bonus-detail-quantity">+100</li> 
                            <li class="bonus-detail-reason"><span class="reason-txt">原因：</span>中國信託ATM－百元酷碰專區活動(6月)</li> 
                        </ul>  
                        <ul class="bonus-list-detail-content">
                            <li class="bonus-detail-date">06/25</li>
                            <li class="bonus-detail-num">-</li>
                            <li class="bonus-detail-quantity">+100</li> 
                            <li class="bonus-detail-reason"><span class="reason-txt">原因：</span>中國信託ATM－百元酷碰專區活動(6月)</li> 
                        </ul>  
                    </div>
                </div>
            </div>

        </div>
        

    </div>
</div>