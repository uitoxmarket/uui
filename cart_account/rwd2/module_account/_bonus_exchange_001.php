<div class="bonus_exchange_001">
    <div class="container">
        <div class="account-title">兌換購物金</div>
        <div class="bonus-exchange-block">
            <form>                
                <div class="bonus-exchange-content">            
                    <dl>
                        <dt><label for="events_sn">輸入活動序號</label></dt>
                        <dd><input type="text" placeholder="活動序號" value="" id="events_sn"></dd>
                    </dl>
                    <dl>
                        <dt><label for="security_code">請輸入驗證碼</label></dt>
                        <dd><input type="text" placeholder="驗證碼" value="" id="security_code"></dd>
                        <dd><img src="../c/img/frame/showrandimg.png" height="20" width="60" class="r-img"></dd>
                        <dd><a href="javascript:void(0);" class="replay">重新產生</a></dd>
                    </dl>            
                </div>

                <div class="actions">
                    <a title="Close" href="account.php" class="button large gray btn-close">取消</a>
                    <a href="javascript:void(0);" class="button large light-blue contact-submit">兌換購物金</a>
                </div> 
            </form>
        </div>
    </div>
</div>