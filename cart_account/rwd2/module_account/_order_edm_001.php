<div class="order_edm_001">
    <div class="container">
        <div class="account-title">取消/訂閱電子報</div>
        <div class="order-edm-block">
            <form>
                <p class="order-edm-info">欲訂閱電子報請打勾，如您不想訂閱，請勿勾選。</p>
                <label><input type="checkbox" name="" value="">訂閱電子報</label>

                <div class="actions">
                    <a title="Close" href="account.php" onclick="$.fancybox.close()" class="button large gray btn-close">取消</a>
                    <a href="javascript:void(0);" class="button large light-blue contact-submit">確認</a>
                </div>
            </form>
        </div>
    </div>
</div>