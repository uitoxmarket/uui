<div class="bonus_info_001">
    <div class="container">
        <div class="account-title">購物金使用說明</div>
        <div class="bonus-info-block">
            <h1>購物金 1 點折抵 NT$1 元，最高折抵購買商品總金額20%，結帳立即折抵！</h1>
            <p>
                購物金是網站不定期舉辦購物金贈送活動，回饋給消費者的福利。<br>
                參與活動所贈送的購物金，會自動發送到購買帳戶，<br>
                購物金生效後，您在網站消費時，即可使用 <span class="text-focus">購物金1點=NT$1元</span> 的比例，折抵結帳金額。<br>

                ∗ 購物金僅能於本網站進行結帳金額折抵，不得共用於其他使用uitox金物流服務的網站，或要求替換成現金、贈品。
            </p>
            
            <ul class="bonus-info-content">
                <li>
                    <h2>使用說明</h2>
                    <p>結帳時，系統將自動計算此次消費可折抵金額，勾選「用購物金折抵」，可立即折抵。</p>
                    <div class="bonus-example">
                        
                        <div class="order-list-content top">
                            <h3 class="order-name">商品名稱</h3>                        
                            <ul class="order-list-r">
                                <li class="order-quantity">數量</li>                                                               
                                <li class="order-price">單價</li>
                                <li class="order-total-s">小計</li>                                 
                            </ul>                                         
                        </div>

                        <div class="order-list-content">
                            <div class="order-images"><img src="https://member-tw1.uitox.com/c/img/member/tw_zh-tw/img_50_06.jpg"></div>
                            <h3 class="order-name">舒潔 優質抽取衛生紙(110抽x8包)</h3>                        
                            <ul class="order-list-r">
                                <li class="order-quantity">
                                    <div class="quantity">
                                        <span class="quantity-decrease">-</span>
                                        <input type="text" class="quantity-input" value="1" max="99" title="請輸入購買量">
                                        <span class="quantity-increase">+</span>
                                    </div>
                                </li>                                                               
                                <li class="order-price">NT$145</li>
                                <li class="order-total-s">NT$145</li>
                            </ul>
                            <div class="order-delete"></div>                                       
                        </div>
                        

                        <div class="order-list-content fee shipping-fee">
                            <h3 class="order-name">運費</h3>                        
                            <ul class="order-list-r">
                                <li class="order-total-s"><span class="currency">NT$</span>80</li>
                            </ul>                                
                        </div>

                        <div class="order-list-content fee bonus-fee">
                            <h3 class="order-name"><label for="check_coupon"><input type="checkbox" id="check_coupon" checked>用購物金折抵 NT$15</label><span>請在框框打勾</span></h3>                        
                            <ul class="order-list-r">
                                <li class="order-total-s">-<span class="currency">NT$</span>15</li>
                            </ul>                 
                        </div>                        

                        <div class="order-list-content fee total-fee">
                            <ul class="order-list-r">                                
                                <li class="order-total-s">總計 <span class="currency">NT$</span> 2,944</li>
                            </ul>
                        </div>

                    </div>
                </li>

                <li>
                    <h2>折抵上限</h2>
                    <p>每次消費最高可折抵結帳商品總金額20%，折抵後低於免運門檻，適用免運費規則。</p>
                </li>
                <li>
                    <h2>購物金有效期限</h2>
                    <p>生效日期加60天為有效期限。</p>
                </li>
                <li>
                    <h2>退訂/退貨歸還購物金</h2>
                    <p>
                        每整筆訂單退訂時，確認商品未出貨且無須退款，購物金將於30分鐘內歸還(原使用效期不變)。
                        商品已出貨，退款時，重新發送該筆使用的購物金。
                        訂單全退，該筆使用的購物金全數退回；訂單部分退，按比例歸還。
                    </p>
                </li>
            </ul> 
            
            <p>查詢購物金使用期限與記錄，可至 <a href="account.php">我的帳戶</a> → <a href="bonus_list.php">查購物金</a></p>

            <p class="tip">新加坡優達斯國際有限公司台灣分公司 保留隨時變更、修改或終止本活動及約定條款之權利，若有異動，修改後的活動內容及約定條款將公佈在本站上，不另外個別通知。</p>
        </div>
    </div>
</div>