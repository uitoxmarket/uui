<div class="order_refund_002">
    <div class="container">
        <div class="order-refund2-block">
            <form>
                <p>此筆訂單尚未進入出貨程序，僅能整筆退訂。<br>(若訂單內尚有您需要的商品，請重新訂購)</p>
                <div class="actions">
                    <a href="../module_component/_site_fancybox_all.php #fancybox-refund-confirm" class="fancybox fancybox.ajax button large gray contact-submit">確認要退</a>
                    <a href="order_detail.php" class="button large light-blue btn-close">不退了</a>                    
                </div>
            </form>
        </div>
    </div>
</div>