<div class="refund_finish_001">
    <div class="container">        
        <div class="order-list-block">
            <div class="o-top-title">
                <h3>已收到您的退訂申請</h3>
                <h2>退訂商品處理、發票折讓單資訊，可至 <a href="order_refund_state.php">退訂進度</a> 查詢</h2>
            </div>      
                
            
            <div class="refund-info">
                <h5 class="o-r-title">退訂金額</h5>
                <ul>
                    <li>退訂金額：<span class="red">NT$1,178</span></li>
                    <li>退款方式：退款至銀行帳戶</li>
                    <li>退款時歸還購物金：<span class="red">179</span></li>
                </ul>                               
            </div>

            <div class="refund-info">
                <h5 class="o-r-title">退訂商品</h5>
                <ul>
                    <li>雙面羔羊絨連帽外套/連帽大衣 - 藍色 - F</li>
                    <li>ICARER 復古系列 iPad mini with Retina 雙折站立 磁扣側掀 手工真皮皮套 - 棕</li>
                </ul>                               
            </div>

            <div class="refund-info">
                <h5 class="o-r-title">退貨說明</h5>
                <p>
                    將有宅配公司與您聯繫，我們會盡力於 <span class="red">3</span> 個工作天內前往取貨，請保持電話暢通，不要拒接不明來電顯示。<br>
                    請將退貨商品完整包裝，連同附件贈品一同退回， 並於外箱貼上退貨寄送單，或將單號抄寫在uitox外箱上。<br>
若無uitox外箱，請您在商品原廠外盒之外，再以其它適當的包裝盒進行包裝。<br>
[切勿直接寫在非uitox外箱，以免影響退貨權益，謝謝您的合作]
                </p>
            </div>

            <div class="refund-info">
                <div class="refund-form-content">
                    <h3>退貨寄送單</h3> 
                    <h2>宅配取貨單號：9273893822</h2>
                    <h4>訂單編號：201502AP03001205</h4>
                    <ul class="r-form-data">
                        <li class="r-form-title">收件人</li>
                        <li class="r-form-detail">
                            <dl>
                                <dt>姓名：</dt>
                                <dd>新加坡商優達斯國際有限公司台灣辦事處倉管營運退貨組</dd>
                            </dl>
                            <dl>
                                <dt>地址：</dt>
                                <dd>uitox倉庫(00156)</dd>
                            </dl>
                        </li>
                    </ul>
                    <ul class="r-form-data">
                        <li class="r-form-title">寄件人</li>
                        <li class="r-form-detail">
                            <dl>
                                <dt>姓名：</dt>
                                <dd>王曉明</dd>
                            </dl>
                            <dl>
                                <dt>地址：</dt>
                                <dd>台北市信義區基隆路一段163號9樓</dd>
                            </dl>
                        </li>
                    </ul>
                </div>                                            
            </div>

            <div class="refund-info">
                <div class="refund-form-content">
                    <h3>退貨寄送單</h3>     
                    <h2>訂單編號：201502AP03001205</h2>
                    <ul class="r-form-data">
                        <li class="r-form-title">收件人</li>
                        <li class="r-form-detail">
                            <dl>
                                <dt>姓名：</dt>
                                <dd>新加坡商優達斯國際有限公司台灣辦事處倉管營運退貨組</dd>
                            </dl>
                            <dl>
                                <dt>地址：</dt>
                                <dd>uitox倉庫(00156)</dd>
                            </dl>
                        </li>
                    </ul>
                    <ul class="r-form-data">
                        <li class="r-form-title">寄件人</li>
                        <li class="r-form-detail">
                            <dl>
                                <dt>姓名：</dt>
                                <dd>王曉明</dd>
                            </dl>
                            <dl>
                                <dt>地址：</dt>
                                <dd>台北市信義區基隆路一段163號9樓</dd>
                            </dl>
                        </li>
                    </ul>
                </div>                                            
            </div>

            <div class="refund-actions">
                <a href="order_list.php" class="button large light-blue btn-close">列印退貨寄送單</a>
                <a href="order_list.php" class="button large light-blue contact-submit">回查訂單</a>
            </div>
                 
        </div>

    </div>
</div>