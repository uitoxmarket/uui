<div class="orderdetail_001">
    <div class="container">        
        <div class="order-list-block">        
            <div class="order-list-top">
                <div class="o-list-top-r">
                    <span class="order-num">訂單編號：201412AP09000123</span>
                </div>
                <div class="o-list-top-l">
                    <ul>                
                        <li class="order-address">收貨資訊： *于*  0922******    110台北市信義區*****段563號8樓<a href="../module_component/_site_fancybox_all.php #fancybox-order-address-change" class="fancybox fancybox.ajax">改</a></li>
                    </ul>
                </div>              
                <div class="o-list-top-menu">
                    <a href="../module_component/_site_fancybox_all.php #fancybox-order-invoice" class="fancybox fancybox.ajax">發票</a>
                    <a href="order_refund.php">退訂</a>
                    <a href="order_refund_2.php">退訂</a>
                    <a href="../module_component/_site_fancybox_all.php #fancybox-refund-cvs" class="fancybox fancybox.ajax">退訂</a>
                    <a href="../module_component/_site_fancybox_all.php #fancybox-refund-expired" class="fancybox fancybox.ajax">退訂</a>
                    <a href="../module_component/_site_fancybox_all.php #fancybox-refund-all" class="fancybox fancybox.ajax">退訂</a>
                </div>
            </div>

            <div class="order-list">
                <div class="order-list-content">
                    <div class="order-sn">01</div>
                    <h3 class="order-name">
                        <a href="#">雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F</a>
                        <div class="order-pre">預計 2015/05/25 出貨</div>
                    </h3>                                            
                    <ul class="order-list-r">
                        <li class="order-shipping"><a href="../module_component/_site_fancybox_all.php #fancybox-order-shipping" class="fancybox fancybox.ajax">出貨進度：已出貨</a></li>
                        <li class="order-quantity">99件</li>
                        <li class="order-price">NT$19,900,000</li>
                        <li class="order-status"><span class="tag-status is-vendor-delivery">廠商出貨</span><a href="../module_component/_site_fancybox_all.php #fancybox-order-vendor" class="fancybox fancybox.ajax">聯絡廠商</a></li>
                        <li class="order-returned">已退：1 (<a href="order_refund_state.php">退訂進度</a>)</li>
                    </ul>
                    <div class="order-list-add o-events">
                        <div class="order-sn"></div>
                        <h3 class="order-name"><a href="../module_component/_site_fancybox_all.php #fancybox-order-event" class="fancybox fancybox.ajax">[活動] XXXX開館慶 (詳情)</a></h3>
                    </div>
                    <div class="order-list-add o-events">
                        <div class="order-sn"></div>
                        <h3 class="order-name"><a href="../module_component/_site_fancybox_all.php #fancybox-order-event" class="fancybox fancybox.ajax">[活動] XXXX開館慶 (詳情)</a></h3>
                    </div>             
                </div>

                <div class="order-list-content group">
                    <div class="order-sn"></div>
                    <h3 class="order-name"><a href="#">CLINIQUE 倩碧 超水感新一代水磁場保濕凝膠 75ml x 2入，包含以下商品：</a></h3>                        
                    <ul class="order-list-r">
                        <li class="order-status"></li>
                        <li class="order-quantity"></li>
                        <li class="order-price">NT$19,900,000</li>
                        <li class="order-shipping"></li>
                        <li class="order-returned"></li>
                    </ul>
                    <div class="order-list-add">
                        <div class="order-sn">02</div>
                        <h3 class="order-name"><a href="#">CLINIQUE 倩碧 超水感 x 2入</a></h3>                        
                        <ul class="order-list-r">
                            <li class="order-shipping"><a href="../module_component/_site_fancybox_all.php #fancybox-order-shipping" class="fancybox fancybox.ajax">出貨進度：已出貨</a></li>
                            <li class="order-quantity">99pcs</li>
                            <li class="order-price"></li>
                            <li class="order-status"><span class="tag-status is-uitox-delivery">uitox出貨</span></li>
                            <li class="order-returned">已退：1 (<a href="order_refund_state.php">退訂進度</a>)</li>
                        </ul>
                    </div>
                    <div class="order-list-add">
                        <div class="order-sn">03</div>
                        <h3 class="order-name"><a href="#">雙面羔羊絨連帽外套/連帽大衣 - 雙面款-F</a></h3>                        
                        <ul class="order-list-r">
                            <li class="order-shipping"><a href="../module_component/_site_fancybox_all.php #fancybox-order-shipping" class="fancybox fancybox.ajax">出貨進度：尚未出貨</a></li>
                            <li class="order-quantity">99件</li>
                            <li class="order-price"></li>
                            <li class="order-status"><span class="tag-status is-uitox-delivery">uitox出貨</span></li>
                            <li class="order-returned">已退：1 (<a href="order_refund_state.php">退訂進度</a>)</li>
                        </ul> 
                    </div>           
                </div>

                <div class="order-list-content">
                    <div class="order-sn">04</div>
                    <h3 class="order-name"><a href="#">加購品 - ICARER 復古系列 iPad mini with Retina 雙折站立 磁扣側掀 手工真皮皮套 - 棕</a></h3>                        
                    <ul class="order-list-r">
                        <li class="order-shipping"><a href="../module_component/_site_fancybox_all.php #fancybox-order-shipping" class="fancybox fancybox.ajax">出貨進度：尚未出貨</a></li>
                        <li class="order-quantity">99件</li>
                        <li class="order-price">NT$19,900,000</li>
                        <li class="order-status"><span class="tag-status is-vendor-delivery">廠商出貨</span><a href="../module_component/_site_fancybox_all.php #fancybox-order-vendor" class="fancybox fancybox.ajax">聯絡廠商</a></li>
                        <li class="order-returned">已退：1 (<a href="order_refund_state.php">退訂進度</a>)</li>
                    </ul>                 
                </div>
                
                <div class="order-list-content refund">
                    <h3 class="order-refund-title">出貨前退訂</h3>                        
                    <div class="order-list-add">
                        <div class="order-sn">05</div>
                        <h3 class="order-name"><a href="#">CLINIQUE 倩碧 超水感 x 2入</a></h3>                        
                        <ul class="order-list-r">
                            <li class="order-shipping"><a href="../module_component/_site_fancybox_all.php #fancybox-order-shipping" class="fancybox fancybox.ajax">出貨進度：已出貨</a></li>
                            <li class="order-quantity">99pcs</li>
                            <li class="order-price">NT$19,900,000</li>
                            <li class="order-status"><span class="tag-status is-uitox-delivery">uitox出貨</span></li>
                            <li class="order-returned"></li>
                        </ul>
                    </div>
                    <div class="order-list-add">
                        <div class="order-sn">06</div>
                        <h3 class="order-name">買立折折扣</h3>                        
                        <ul class="order-list-r">
                            <li class="order-shipping"></li>
                            <li class="order-quantity"></li>
                            <li class="order-price">- NT$18</li>
                            <li class="order-status"></li>
                            <li class="order-returned"></li>
                        </ul> 
                    </div>           
                </div>

                <div class="fee">
                    <div class="order-list-content">
                        <h3 class="order-name">活動折扣(XXXX開館慶)</h3>                        
                        <ul class="order-list-r">
                            <li class="order-price">-NT$10</li>
                        </ul>                 
                    </div>
                    <div class="order-list-content">
                        <h3 class="order-name">購物金折扣</h3>                        
                        <ul class="order-list-r">
                            <li class="order-price">-NT$100</li>
                        </ul>                 
                    </div>
                </div>
                
                <div class="shipping-fee">
                    <div class="order-list-content">
                        <h3 class="order-name">Payment Code</h3>                        
                        <ul class="order-list-r">
                            <li class="order-price">Rp 892</li>
                        </ul>                                
                    </div>
                    <div class="order-list-content">
                        <h3 class="order-name">運費</h3>                        
                        <ul class="order-list-r">
                            <li class="order-price">NT$80</li>
                        </ul>                                
                    </div>
                    <div class="order-list-content">
                        <h3 class="order-name">日本處理費</h3>                        
                        <ul class="order-list-r">
                            <li class="order-price">NT$200</li>
                        </ul>                                
                    </div>
                </div>

                <div class="order-tax">
                    <div class="order-list-content">
                        <h3 class="order-name">Total before Tax</h3>                        
                        <ul class="order-list-r">
                            <li class="order-price">US$361.79</li>
                        </ul>                                
                    </div>
                    <div class="order-list-content">
                        <h3 class="order-name">Sales Tax</h3>                        
                        <ul class="order-list-r">
                            <li class="order-price">US$20.20</li>
                        </ul>                                
                    </div>
                    <div class="order-list-content">
                        <h3 class="order-name">Recycle fee</h3>                        
                        <ul class="order-list-r">
                            <li class="order-price">US$7.00</li>
                        </ul>                                
                    </div>
                </div>
                
                <div class="total-fee">
                    <div class="order-list-content">
                        <h3 class="order-name">訂單總計</h3>
                        <ul class="order-list-r">
                            <li class="order-price">NT$22,944,944</li>
                        </ul>
                        <div class="order-list-add o-pay-way">
                            <h4 class="order-name">付款方式：永豐銀行信用卡一次付清 (信用卡紅利點數折抵)</h4>
                        </div>                        
                        <div class="order-list-add">
                            <h4 class="order-name red">折抵點數3000點 = 180元<br>(180元於信用卡帳單內扣除，不直接折抵刷卡金額)</h4>
                        </div>
                        <div class="order-list-add">
                            <h4 class="order-name">活動說明：已符合 XXXX開館慶，將贈送給您購物金200點，<br>贈送點數將於2015/03/03給點，商品退貨未達門檻將取消給點。</h4>
                        </div>                                  
                    </div>
                    <div class="order-list-content">
                        <h3 class="order-name">訂單總計</h3>
                        <ul class="order-list-r">
                            <li class="order-price">NT$2,944</li>
                        </ul>
                        <div class="order-list-add o-pay-way">
                            <h4 class="order-name">付款方式：台新銀行信用卡分期6期0利率 (每期約$490)</h4>
                        </div> 
                    </div>
                    <div class="order-list-content">
                        <h3 class="order-name">訂單總計</h3>
                        <ul class="order-list-r">
                            <li class="order-price">NT$2,944</li>
                        </ul>
                        <div class="order-list-add o-pay-way">
                            <h4 class="order-name">付款方式：ATM轉帳</h4>
                            <div class="o-atm-content">
                                <dl>
                                    <dt>銀行：</dt>
                                    <dd>國泰世華 013</dd>
                                </dl>
                                <dl>
                                    <dt>帳號：</dt>
                                    <dd>2911150218883640</dd>
                                </dl>
                                <dl>
                                    <dt>金額：</dt>
                                    <dd>NT$2,944</dd>
                                </dl>
                                <dl>
                                    <dt>繳款期限：</dt>
                                    <dd>2015/06/26 23:59</dd>
                                </dl>
                            </div>
                        </div>                       
                    </div>
                </div>
                    

            </div>
            
            <div  class="order-help">
                <h5>需要協助?</h5>
                <a href="../module_component/_site_fancybox_all.php #fancybox-order-contact" class="fancybox fancybox.ajax">沒收到貨</a>
                <a href="../module_component/_site_fancybox_all.php #fancybox-order-product-problem" class="fancybox fancybox.ajax">貨到有缺/損壞/送錯</a>
                <a href="../module_component/_site_fancybox_all.php #fancybox-order-warranty" class="fancybox fancybox.ajax">維修保固資訊</a>
                <a href="../module_component/_site_fancybox_all.php #fancybox-order-contact" class="fancybox fancybox.ajax">發票問題</a>
                <a href="../module_component/_site_fancybox_all.php #fancybox-order-contact" class="fancybox fancybox.ajax">聯絡客服</a>
                <a href="../module_component/_site_fancybox_all.php #fancybox-order-contact" class="fancybox fancybox.ajax">逾時申訴</a>                
            </div>
                 
        </div>

    </div>
</div>