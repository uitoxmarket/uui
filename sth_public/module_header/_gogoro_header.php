<header class="site-header">
    <div class="container">
        <a href="index.php" class="logo"><img src="../c/img/sth_gogoro/logo.svg" alt=""></a>
        <nav class="nav-user">
            <ul>
                <li><div class="locate"><a href="#" class="active">English</a><a href="#">中文</a></div></li>
                <li>
                    <a class="#"><span class="count">1</span> Cart</a>
                </li>
                <li><a href="#">My Gogoro</a></li>
                <li><a href="" class="icon-menu"></a></li>
            </ul>
        </nav>
    </div>
</header>
<nav class="nav-menu">
    <div class="container">
        <ul class="menu-one">
            <li>
                <a class="active" href="../<?php echo $siteName;?>/category.php"><img src="../c/img/sth_gogoro/icon_menu_01.png" alt=""><span class="name">verview</span></a>
            </li>
            <li>
                <a class="active" href="../<?php echo $siteName;?>/category.php"><img src="../c/img/sth_gogoro/icon_menu_02.png" alt=""><span class="name">Scooters</span></a>
            </li>
            <li>
                <a class="active" href="../<?php echo $siteName;?>/category.php"><img src="../c/img/sth_gogoro/icon_menu_03.png" alt=""><span class="name">Accessories</span></a>
            </li>
            <li>
                <a class="active" href="../<?php echo $siteName;?>/category.php"><img src="../c/img/sth_gogoro/icon_menu_04.png" alt=""><span class="name">Merchandise</span></a>
            </li>
            <li>
                <a class="active" href="../<?php echo $siteName;?>/category.php"><img src="../c/img/sth_gogoro/icon_menu_05.png" alt=""><span class="name">App Extras</span></a>
            </li>
        </ul>
    </div>
</nav>