<?php

$rwd_index_001 = array(
    array(
        "name"      => "輪播圖1",
        "images"    => "../c/img/sth_stayreal/slide_01.jpg",
        "url"       => "item.php",
    ),
    array(
        "name"      => "輪播圖1",
        "images"    => "../c/img/sth_stayreal/slide_02.jpg",
        "url"       => "item.php",
    ),
    array(
        "name"      => "輪播圖1",
        "images"    => "../c/img/sth_stayreal/slide_03.jpg",
        "url"       => "item.php",
    ),
    array(
        "name"      => "輪播圖1",
        "images"    => "../c/img/sth_stayreal/slide_04.jpg",
        "url"       => "item.php",
    ),
);

$rwd_index_009_title = "熱賣商品";

$rwd_index_009 = array(
    array(
        "name"      => "Liverpool Denim Shirt 利物浦丹寧襯衫",
        "images"    => "../c/img/sth_stayreal/prod_01.jpg",
        "images-full"    => "http://placehold.it/600x600",
        "url"       => "../front/item.php",
        "price"     => "1,980",
        "stock"     => true,
        "type"      => "",
        "desc"      => "1960年代，傳奇性搖滾團體披頭四於英國「利物浦」誕生 ，影響了熱愛音樂的你，甚至全世界。這個神祕靠海的小鎮於2008年被喻為「歐洲文化之都」，蘊藏豐富的藝術文化氣息。擁有利物浦系列，下一個搖滾之星就是你。",
        "feature"   => array(
            "丹寧布料舒適立挺",
            "率性不羈的刷色效果",
            "初秋不敗穿搭單品",
        ),
        "gift"   => array(
            "薯條小兔公仔",
        ),
    ),
    array(
        "name"      => "Hi！Leopard Mousy T-shirt 嗨！豹豹寬版T(紫標版型/白色)",
        "images"    => "../c/img/sth_stayreal/prod_13.jpg", 
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "時尚潮流不敗設計元素-「豹紋」 強勢來襲，炙手可熱的豹紋流行符號，STAYREAL創意呈現！發揮獨特創意打造小鼠cosplay成為豹豹造型。",
        "feature"   => array(
            "時尚潮流不敗設計",
            "「豹紋」 強勢來襲",
            "獨特創意打造小鼠cosplay",
        ),
    ),
    array(
        "name"      => "Sporty Rock！Denim Coat 動次！牛仔外套(紅標版型/藍色)",
        "images"    => "../c/img/sth_stayreal/prod_03.jpg",
        "url"       => "../front/item.php",
        "price"     => "2,480",
        "stock"     => true,
        "type"      => "",
        "desc"      => "牛仔外套為女孩夏日輕鬆休閒的絕佳單品以設計感較為柔和的牛仔布著手，舒適輕盈的牛仔外套展現女孩搶眼個性！",
        "feature"   => array(
            "夏日輕鬆休閒牛仔外套",
            "絕佳單品設計感",
            "舒適輕盈的搶眼個性",
        ),
    ),
    array(
        "name"      => "DORAEMON X STAYREAL Panda T-shirt 哆啦熊貓T(黑標版型/黑色)",
        "images"    => "../c/img/sth_stayreal/prod_14.jpg",
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "驚！到底是哆啦還是熊貓？傻傻分不清楚！ STAYREAL結合四川成都最具代表性的『熊貓』吉祥物 首度將經典卡通人物及熊貓做完美結合 哆啦cosplay熊貓 驚嚇超萌樣超吸睛 創意感十足！",
        "feature"   => array(
            "四川成都『熊貓』吉祥物",
            "哆啦cosplay熊貓",
            "驚嚇超萌樣超吸睛",
        ),
    ),
    array(
        "name"      => "Sporty Rock！Dots T-shirt 動次！點點長版T(紫標版型/黑色)",
        "images"    => "../c/img/sth_stayreal/prod_05.jpg",
        "url"       => "../front/item.php",
        "price"     => "1,680",
        "stock"     => true,
        "type"      => "",
        "desc"      => "一件式連身運動休閒寬版T，讓妳輕鬆自在的活動，點點圖騰設計點綴整體造型氛圍，著用寬版剪裁更能修飾身型！",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
    ),
    array(
        "name"      => "Sporty Rock！Baseball T-shirt 動次！長版棒球衫(紫標版型/黑色)",
        "images"    => "../c/img/sth_stayreal/prod_06.jpg",
        "url"       => "../front/item.php",
        "price"     => "1,980",
        "stock"     => true,
        "type"      => "",
        "desc"      => "今年夏日運動風必備棒球衫，女孩搖身一變 成為休閒動時尚代表，讓一成不變的造型變得更加時髦有型！",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
    ),
    array(
        "name"      => "Liverpool Rider Coat 利物浦女騎士風衣 (紅標版型/紅色)",
        "images"    => "../c/img/sth_stayreal/prod_15.jpg",
        "url"       => "../front/item.php",
        "price"     => "3,480",
        "stock"     => true,
        "type"      => "",
        "desc"      => "女孩騎士風衣設計不失女孩優雅，俐落剪裁能修飾比例，布料經防風及防潑水處理，帶出女孩個性與服裝機能性。",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
    ),
    array(
        "name"      => "Liverpool Rider Coat 利物浦騎士風衣(黑標版型/深藍)",
        "images"    => "../c/img/sth_stayreal/prod_08.jpg",
        "url"       => "../front/item.php",
        "price"     => "3,480",
        "stock"     => true,
        "type"      => "",
        "desc"      => "1960年代，傳奇性搖滾團體披頭四於英國「利物浦」誕生 ，影響了熱愛音樂的你，甚至全世界。這個神祕靠海的小鎮於2008年被喻為「歐洲文化之都」，蘊藏豐富的藝術文化氣息。擁有利物浦系列，下一個搖滾之星就是你。",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
    ),
    array(
        "name"      => "England Trench Coat 英式雙排釦風衣(黑標版型/黑色)",
        "images"    => "../c/img/sth_stayreal/prod_16.jpg",
        "url"       => "../front/item.php",
        "price"     => "3,880",
        "stock"     => true,
        "type"      => "",
        "desc"      => "放鬆心情，帶自己來一趟異國之旅吧…..！
設計氣息帶有英倫格調，保有異國風情，再搭配高時尚度的雙排釦，增添造型感，紙飛機刺繡展現獨有的質感氣息，讓大衣的層次感在細節中展現。",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
    ),
    array(
        "name"      => "Military Stand-up Collar Coat 輕軍事立領外套(黑標版型/綠色)",
        "images"    => "../c/img/sth_stayreal/prod_10.jpg",
        "url"       => "../front/item.php",
        "price"     => "3,680",
        "stock"     => true,
        "type"      => "",
        "desc"      => "簡單俐落的線條，屬於軍事的味道，魔鬼藏在細節剪裁中。隨著時間的洗鍊，細膩的收邊及專屬軍事的硬挺材質呈現不羈的質感。",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
    ),
);

$itemss = array(
    array(
        "name"      => "Liverpool Denim Shirt 利物浦丹寧襯衫",
        "images"    => "../c/img/sth_stayreal/prod_01.jpg",
        "images-full"    => "http://placehold.it/600x600",
        "url"       => "../front/item.php",
        "price"     => "1,980",
        "stock"     => true,
        "type"      => "",
        "desc"      => "1960年代，傳奇性搖滾團體披頭四於英國「利物浦」誕生 ，影響了熱愛音樂的你，甚至全世界。這個神祕靠海的小鎮於2008年被喻為「歐洲文化之都」，蘊藏豐富的藝術文化氣息。擁有利物浦系列，下一個搖滾之星就是你。",
        "feature"   => array(
            "丹寧布料舒適立挺",
            "率性不羈的刷色效果",
            "初秋不敗穿搭單品",
        ),
        "gift"   => array(
            "薯條小兔公仔",
        ),
    ),
    array(
        "name"      => "Hi！Leopard Mousy T-shirt 嗨！豹豹寬版T(紫標版型/白色)",
        "images"    => "../c/img/sth_stayreal/prod_02.jpg", 
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "時尚潮流不敗設計元素-「豹紋」 強勢來襲，炙手可熱的豹紋流行符號，STAYREAL創意呈現！發揮獨特創意打造小鼠cosplay成為豹豹造型。",
        "feature"   => array(
            "時尚潮流不敗設計",
            "「豹紋」 強勢來襲",
            "獨特創意打造小鼠cosplay",
        ),
    ),
    array(
        "name"      => "Sporty Rock！Denim Coat 動次！牛仔外套(紅標版型/藍色)",
        "images"    => "../c/img/sth_stayreal/prod_03.jpg",
        "url"       => "../front/item.php",
        "price"     => "2,480",
        "stock"     => true,
        "type"      => "",
        "desc"      => "牛仔外套為女孩夏日輕鬆休閒的絕佳單品以設計感較為柔和的牛仔布著手，舒適輕盈的牛仔外套展現女孩搶眼個性！",
        "feature"   => array(
            "夏日輕鬆休閒牛仔外套",
            "絕佳單品設計感",
            "舒適輕盈的搶眼個性",
        ),
    ),
    array(
        "name"      => "DORAEMON X STAYREAL Panda T-shirt 哆啦熊貓T(黑標版型/黑色)",
        "images"    => "../c/img/sth_stayreal/prod_04.jpg",
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "驚！到底是哆啦還是熊貓？傻傻分不清楚！ STAYREAL結合四川成都最具代表性的『熊貓』吉祥物 首度將經典卡通人物及熊貓做完美結合 哆啦cosplay熊貓 驚嚇超萌樣超吸睛 創意感十足！",
        "feature"   => array(
            "四川成都『熊貓』吉祥物",
            "哆啦cosplay熊貓",
            "驚嚇超萌樣超吸睛",
        ),
    ),
    array(
        "name"      => "Sporty Rock！Dots T-shirt 動次！點點長版T(紫標版型/黑色)",
        "images"    => "../c/img/sth_stayreal/prod_05.jpg",
        "url"       => "../front/item.php",
        "price"     => "1,680",
        "stock"     => true,
        "type"      => "",
        "desc"      => "一件式連身運動休閒寬版T，讓妳輕鬆自在的活動，點點圖騰設計點綴整體造型氛圍，著用寬版剪裁更能修飾身型！",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
    ),
    array(
        "name"      => "Sporty Rock！Baseball T-shirt 動次！長版棒球衫(紫標版型/黑色)",
        "images"    => "../c/img/sth_stayreal/prod_06.jpg",
        "url"       => "../front/item.php",
        "price"     => "1,980",
        "stock"     => true,
        "type"      => "",
        "desc"      => "今年夏日運動風必備棒球衫，女孩搖身一變 成為休閒動時尚代表，讓一成不變的造型變得更加時髦有型！",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
    ),
    array(
        "name"      => "Liverpool Rider Coat 利物浦女騎士風衣 (紅標版型/紅色)",
        "images"    => "../c/img/sth_stayreal/prod_07.jpg",
        "url"       => "../front/item.php",
        "price"     => "3,480",
        "stock"     => true,
        "type"      => "",
        "desc"      => "女孩騎士風衣設計不失女孩優雅，俐落剪裁能修飾比例，布料經防風及防潑水處理，帶出女孩個性與服裝機能性。",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
    ),
    array(
        "name"      => "Liverpool Rider Coat 利物浦騎士風衣(黑標版型/深藍)",
        "images"    => "../c/img/sth_stayreal/prod_08.jpg",
        "url"       => "../front/item.php",
        "price"     => "3,480",
        "stock"     => true,
        "type"      => "",
        "desc"      => "1960年代，傳奇性搖滾團體披頭四於英國「利物浦」誕生 ，影響了熱愛音樂的你，甚至全世界。這個神祕靠海的小鎮於2008年被喻為「歐洲文化之都」，蘊藏豐富的藝術文化氣息。擁有利物浦系列，下一個搖滾之星就是你。",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
    ),
    array(
        "name"      => "England Trench Coat 英式雙排釦風衣(黑標版型/黑色)",
        "images"    => "../c/img/sth_stayreal/prod_09.jpg",
        "url"       => "../front/item.php",
        "price"     => "3,880",
        "stock"     => true,
        "type"      => "",
        "desc"      => "放鬆心情，帶自己來一趟異國之旅吧…..！
設計氣息帶有英倫格調，保有異國風情，再搭配高時尚度的雙排釦，增添造型感，紙飛機刺繡展現獨有的質感氣息，讓大衣的層次感在細節中展現。",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
    ),
    array(
        "name"      => "Military Stand-up Collar Coat 輕軍事立領外套(黑標版型/綠色)",
        "images"    => "../c/img/sth_stayreal/prod_10.jpg",
        "url"       => "../front/item.php",
        "price"     => "3,680",
        "stock"     => true,
        "type"      => "",
        "desc"      => "簡單俐落的線條，屬於軍事的味道，魔鬼藏在細節剪裁中。隨著時間的洗鍊，細膩的收邊及專屬軍事的硬挺材質呈現不羈的質感。",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
    ),
);

$rwd_index_006_title = "週邊配件";
$rwd_index_004_title = "NEW ITEMS 新商品";

$rwd_index_006 = array(
    array(
        "name"      => "商品名稱",
        "images"    => "../c/img/sth_stayreal/prod_01.jpg",
        "images-full"    => "http://placehold.it/600x600",
        "url"       => "../front/item.php",
        "price"     => "8,888",
        "stock"     => true,
        "type"      => "btn-preorder",
        "desc"      => "商品描述商品描述商品描述商品描述",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
        "gift"   => array(
            "贈品1",
            "贈品2",
            "贈品3",
        ),
    ),
    array(
        "name"      => "商品名稱",
        "images"    => "../c/img/sth_stayreal/prod_13.jpg",
        "images-full"    => "http://placehold.it/600x600",
        "url"       => "../front/item.php",
        "price"     => "8,888",
        "stock"     => true,
        "type"      => "btn-preorder",
        "desc"      => "商品描述商品描述商品描述商品描述",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
        "gift"   => array(
            "贈品1",
            "贈品2",
            "贈品3",
        ),
    ),
    array(
        "name"      => "商品名稱",
        "images"    => "../c/img/sth_stayreal/prod_03.jpg",
        "images-full"    => "http://placehold.it/600x600",
        "url"       => "../front/item.php",
        "price"     => "8,888",
        "stock"     => true,
        "type"      => "btn-preorder",
        "desc"      => "商品描述商品描述商品描述商品描述",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
        "gift"   => array(
            "贈品1",
            "贈品2",
            "贈品3",
        ),
    ),
    array(
        "name"      => "商品名稱",
        "images"    => "../c/img/sth_stayreal/prod_14.jpg",
        "images-full"    => "http://placehold.it/600x600",
        "url"       => "../front/item.php",
        "price"     => "8,888",
        "stock"     => true,
        "type"      => "btn-preorder",
        "desc"      => "商品描述商品描述商品描述商品描述",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
        "gift"   => array(
            "贈品1",
            "贈品2",
            "贈品3",
        ),
    ),
    array(
        "name"      => "商品名稱",
        "images"    => "../c/img/sth_stayreal/prod_05.jpg",
        "images-full"    => "http://placehold.it/600x600",
        "url"       => "../front/item.php",
        "price"     => "8,888",
        "stock"     => true,
        "type"      => "btn-preorder",
        "desc"      => "商品描述商品描述商品描述商品描述",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
        "gift"   => array(
            "贈品1",
            "贈品2",
            "贈品3",
        ),
    ),
);

$items_color = array(
    array(
        "name"      => "白色",
        "url"       => "../c/img/sth001/sample/color_01.jpg",
    ),
    array(
        "name"      => "粉紅色",
        "url"       => "../c/img/sth001/sample/color_02.jpg",
    ),
    array(
        "name"      => "黃色",
        "url"       => "../c/img/sth001/sample/color_03.jpg",
    ),
    array(
        "name"      => "紫色",
        "url"       => "../c/img/sth001/sample/color_04.jpg",
    ),
    array(
        "name"      => "媚綠色",
        "url"       => "../c/img/sth001/sample/color_05.jpg",
    ),
);



$cart_items = array(
    array(
        "name"      => "Liverpool Denim Shirt 利物浦丹寧襯衫",
        "images"    => "../c/img/sth_stayreal/prod_01.jpg",
        "images-full"    => "http://placehold.it/600x600",
        "url"       => "../front/item.php",
        "price"     => "1,980",
        "stock"     => true,
        "type"      => "",
        "desc"      => "1960年代，傳奇性搖滾團體披頭四於英國「利物浦」誕生 ，影響了熱愛音樂的你，甚至全世界。這個神祕靠海的小鎮於2008年被喻為「歐洲文化之都」，蘊藏豐富的藝術文化氣息。擁有利物浦系列，下一個搖滾之星就是你。",
        "feature"   => array(
            "丹寧布料舒適立挺",
            "率性不羈的刷色效果",
            "初秋不敗穿搭單品",
        ),
        "gift"   => array(
            "贈品1",
            "贈品2",
            "贈品3",
        ),
    ),
    array(
        "name"      => "Hi！Leopard Mousy T-shirt 嗨！豹豹寬版T(紫標版型/白色)",
        "images"    => "../c/img/sth_stayreal/prod_02.jpg", 
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "時尚潮流不敗設計元素-「豹紋」 強勢來襲，炙手可熱的豹紋流行符號，STAYREAL創意呈現！發揮獨特創意打造小鼠cosplay成為豹豹造型。",
        "feature"   => array(
            "時尚潮流不敗設計",
            "「豹紋」 強勢來襲",
            "獨特創意打造小鼠cosplay",
        ),
    ),
    array(
        "name"      => "Sporty Rock！Denim Coat 動次！牛仔外套(紅標版型/藍色)",
        "images"    => "../c/img/sth_stayreal/prod_03.jpg",
        "url"       => "../front/item.php",
        "price"     => "2,480",
        "stock"     => true,
        "type"      => "",
        "desc"      => "牛仔外套為女孩夏日輕鬆休閒的絕佳單品以設計感較為柔和的牛仔布著手，舒適輕盈的牛仔外套展現女孩搶眼個性！",
        "feature"   => array(
            "夏日輕鬆休閒牛仔外套",
            "絕佳單品設計感",
            "舒適輕盈的搶眼個性",
        ),
    ),
    array(
        "name"      => "DORAEMON X STAYREAL Panda T-shirt 哆啦熊貓T(黑標版型/黑色)",
        "images"    => "../c/img/sth_stayreal/prod_04.jpg",
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "驚！到底是哆啦還是熊貓？傻傻分不清楚！ STAYREAL結合四川成都最具代表性的『熊貓』吉祥物 首度將經典卡通人物及熊貓做完美結合 哆啦cosplay熊貓 驚嚇超萌樣超吸睛 創意感十足！",
        "feature"   => array(
            "四川成都『熊貓』吉祥物",
            "哆啦cosplay熊貓",
            "驚嚇超萌樣超吸睛",
        ),
    ),
);

$recommends = array(
    "小鼠系列",
    "RockCOCO",
);

?>