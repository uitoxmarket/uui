<nav class="nav-menu">
	<a href="" class="icon-menu"></a>
	<ul class="menu-one">
		<li>
			<a href="../<?php echo $siteName;?>/category.php">T-SHIRT</a>
			<ul class="menu-two hide">
				<li><a href="#">短袖 T-shirt</a></li>
				<li><a href="#">長袖 T-shirt</a></li>
				<li><a href="#">連帽 T-shirt</a></li>
			</ul>
		</li>
		<li>
			<a href="../<?php echo $siteName;?>/category.php">上衣</a>
			<ul class="menu-two hide">
				<li><a href="#">襯衫</a></li>
				<li><a href="#">POLO衫</a></li>
				<li><a href="#">外套/風衣/帽夾</a></li>
				<li><a href="#">針織衫</a></li>
				<li><a href="#">背心</a></li>
				<li><a href="#">洋裝</a></li>
			</ul>
		</li>
		<li>
			<a href="../<?php echo $siteName;?>/category.php">下身</a>
			<ul class="menu-two hide">
				<li><a href="#">長褲</a></li>
				<li><a href="#">短褲</a></li>
				<li><a href="#">裙子</a></li>
				<li><a href="#">內搭褲</a></li>
				<li><a href="#">鞋子</a></li>
				<li><a href="#">襪子</a></li>
			</ul>
		</li>
		<li>
			<a href="../<?php echo $siteName;?>/category.php">包款</a>
			<ul class="menu-two hide">
				<li><a href="#">後背包</a></li>
				<li><a href="#">側背包</a></li>
				<li><a href="#">肩背包</a></li>
				<li><a href="#">腰包</a></li>
				<li><a href="#">皮夾</a></li>
				<li><a href="#">環保袋</a></li>
			</ul>
		</li>
		<li>
			<a href="../<?php echo $siteName;?>/category.php">配件</a>
			<ul class="menu-two hide">
				<li><a href="#">手套</a></li>
				<li><a href="#">帽子</a></li>
				<li><a href="#">手環/護腕</a></li>
				<li><a href="#">圍巾</a></li>
				<li><a href="#">腰帶</a></li>
				<li><a href="#">飾品</a></li>
			</ul>
		</li>
		</li>
		<li>
			<a href="../<?php echo $siteName;?>/category.php">生活風格</a>
			<ul class="menu-two hide">
				<li><a href="#">公仔</a></li>
				<li><a href="#">手機殼</a></li>
				<li><a href="#">iphone case</a></li>
				<li><a href="#">hTC 手機殼</a></li>
				<li><a href="#">SAMSUNG 手機殼</a></li>
				<li><a href="#">3C保護套/3C週邊</a></li>
				<li><a href="#">杯子/保溫瓶</a></li>
				<li><a href="#">居家生活</a></li>
				<li><a href="#">書籍</a></li>
				<li><a href="#">文具</a></li>
				<li><a href="#">雨傘</a></li>
				<li><a href="#">禮盒</a></li>
				<li><a href="#">鑰匙圈/吊飾</a></li>
			</ul>
		</li>
	</ul>
</nav>