<?php
$rwd_index_001 = array(
    array(
        "name"      => "輪播圖1",
        "images"    => "../c/img/sth_lulu/sample/1220x550-1.jpg",
        "url"       => "item.php",
    ),
    array(
        "name"      => "輪播圖1",
        "images"    => "http://placehold.it/1220x550",
        "url"       => "item.php",
    ),
    array(
        "name"      => "輪播圖1",
        "images"    => "http://placehold.it/1220x550",
        "url"       => "item.php",
    ),
);

$rwd_index_002 = array(
    array(
        "name"      => "商品名稱",
        "images"    => "../c/img/sth_lulu/sample/600x300-1.jpg",
        "url"       => "../front/item.php",
        "price"     => "8,888",
        "stock"     => true,
        "type"      => "btn-preorder",
        "desc"      => "商品描述商品描述商品描述商品描述",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
        "gift"   => array(
            "贈品1",
            "贈品2",
            "贈品3",
        ),
    ),
    array(
        "name"      => "商品名稱",
        "images"    => "../c/img/sth_lulu/sample/600x300-2.jpg",
        "url"       => "../front/item.php",
        "price"     => "8,888",
        "stock"     => true,
        "type"      => "btn-preorder",
        "desc"      => "商品描述商品描述商品描述商品描述",
        "feature"   => array(
            "特色1特色1特色1特色1特色1",
            "特色2特色2特色2特色2特色2",
            "特色3特色3特色3特色3特色3",
        ),
        "gift"   => array(
            "贈品1",
            "贈品2",
            "贈品3",
        ),
    ),
);

$itemss = array(
    array(
        "name"      => "SHOPPER",
        "images"    => "../c/img/sth_lulu/sample/lulu_item_01.jpg",
        "url"       => "../front/item.php",
        "price"     => "8888",
        "stock"     => true,
        "type"      => "btn-addcart", //加入購物車
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "國外進口預購品牌正品",
            "震撼世界",
            "旗艦風尚 潮我看",
        ),
        "gift"   => array(
            "贈品1",
            "贈品2",
            "贈品3",
        ),
    ),
    array(
        "name"      => "FLORA BACKPACK",
        "images"    => "../c/img/sth_lulu/sample/lulu_item_02.jpg",
        "images-full"    => "http://placehold.it/600x600",
        "url"       => "../front/item.php",
        "price"     => "8888",
        "stock"     => true,
        "type"      => "btn-preorder", //立刻預訂
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
        "gift"   => array(
            "薯條小兔公仔",
        ),
    ),
    array(
        "name"      => "SMALL EDIE",
        "images"    => "../c/img/sth_lulu/sample/lulu_item_03.jpg",
        "url"       => "../front/item.php",
        "price"     => "8888",
        "stock"     => true,
        "type"      => "btn-discount", //買立折
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
    array(
        "name"      => "LARGE AMELIA",
        "images"    => "../c/img/sth_lulu/sample/lulu_item_04.jpg",
        "url"       => "../front/item.php",
        "price"     => "8888",
        "stock"     => true,
        "type"      => "btn-rush", //立即搶購
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
    array(
        "name"      => "HANDS BACKPACK",
        "images"    => "../c/img/sth_lulu/sample/lulu_item_01.jpg",
        "url"       => "../front/item.php",
        "price"     => "8888",
        "stock"     => true,
        "type"      => "btn-select", //請選擇尺寸
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
    array(
        "name"      => "SMALL PAULA",
        "images"    => "../c/img/sth_lulu/sample/lulu_item_02.jpg",
        "url"       => "../front/item.php",
        "price"     => "8888",
        "stock"     => true,
        "type"      => "btn-rush-finish", //搶購一空
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
    array(
        "name"      => "SMALL IZZY SATCHEL",
        "images"    => "../c/img/sth_lulu/sample/lulu_item_03.jpg", 
        "url"       => "../front/item.php",
        "price"     => "8888",
        "stock"     => true,
        "type"      => "btn-preorder-finish", //預購結束
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
    array(
        "name"      => "MEDIUM NICOLA",
        "images"    => "../c/img/sth_lulu/sample/lulu_item_04.jpg",
        "url"       => "../front/item.php",
        "price"     => "8888",
        "stock"     => true,
        "type"      => "btn-soldout-stock", //售完，補貨中
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
    array(
        "name"      => "SMALL NICOLA",
        "images"    => "../c/img/sth_lulu/sample/lulu_item_01.jpg",
        "url"       => "../front/item.php",
        "price"     => "8888",
        "stock"     => true,
        "type"      => "btn-soldout", //已售完
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
    array(
        "name"      => "LARGE NICOLA",
        "images"    => "../c/img/sth_lulu/sample/lulu_item_02.jpg",
        "url"       => "../front/item.php",
        "price"     => "8888",
        "stock"     => true,
        "type"      => "btn-to-sale", //即將開賣
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
);

$itemss_single = array(
    array(
        "images"    => "../c/img/sth_lulu/sample/handbags_01.jpg",
    ),
    array(
        "images"    => "../c/img/sth_lulu/sample/handbags_01_2.jpg",
    ),
    array(
        "images"    => "../c/img/sth_lulu/sample/handbags_01_3.jpg",
    ),
    array(
        "images"    => "../c/img/sth_lulu/sample/handbags_01_4.jpg",
    ),
    array(
        "images"    => "../c/img/sth_lulu/sample/handbags_01_5.jpg",
    ),
);

$category_items = $itemss;

$cart_items = $itemss;

$logo = array(
    "images"    => "../c/img/sth_lulu/logo.png",
);




?>