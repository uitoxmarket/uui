<div class="item-spec">
    <div class="container">
        <div class="spec">
            <h3 class="title-h3">Product Spec: </h3>
            <ul>
                <li>Support two SIM card</li>
                <li>Support WCDMA + GSM</li>
                <li>5 inch IPS touch screen, 1,280 x 720 pixels resolution</li>
                <li>Android™ with HTC Sense™</li>
                <li>Intel Atom Z2580, 2GHz Dual core four threads</li>
                <li>800 million pixel SONY BSI Camera & LED light / 200 million pixel front camera</li>
                <li>Support PixelMaster Technology / SonicMaster Sound skill / GloveTouch gloves mode</li>
            </ul>
        </div>
        <div class="spec">
            <h3 class="title-h3">Standard Accessories</h3>
            <ul>
                <li>HTC ONE M8</li>
                <li>Micro-USB 2.0</li>
                <li>transformer</li>
                <li>3.5mm Ear phone</li>
                <li>Warrenty Card</li>
                <li>Chinese, English operation manual</li>
            </ul>
        </div>
        <div class="spec">
            <h3 class="title-h3">Warrenty</h3>
            <ul>
                <li>HTC ONE M8</li>
                <li>Warrenty range: 1 year</li>
                <li>Website Service: <a href="#" target="_blank">offical website</a></li>
                <li>Phone Service: 01234-5678</li>
            </ul>
        </div>
        <div class="spec">
            <h3 class="title-h3">SendTo & Contact Info</h3>
            <ul>
                <li>Send Time: Taipei City 5 hour, Taiwan 24 hours, weekend still available.<a href="">exception</a></li>
                <li>Delivery method: home delivery or 7-11</li>
                <li>Delivery area: Taiwan</li>
            </ul>
        </div>
        <div class="spec">
            <h3 class="title-h3">Return & Refund explain</h3>
            <ul>
                <li>Full stop shopping Jieke enjoy the rights and interests of arrival of goods hesitation period of seven days, if the received merchandise have any questions, direct return to seven days.</li>
                <li>Goods returned must be new and complete package (including but not limited to products, accessories, packaging, carton manufacturers and all accompanying documents or completeness of the information). Otherwise it will affect the cost of your return or require the exercise of the rights and interests of the burden of destruction.</li>
                <li>If you need a replacement, please return to the site, and re-orders.</li>
            </ul>
        </div>
    </div>
</div>