<?php

$nav_breadcrumbs = array(
    "home"      =>  "Home",
    "locate"    =>  "Current locate: ",
    "now"       =>  "Accessories+",
);

$sort = array(
    "default"       => "default sort",
    "sold"          => "sort by sold",
    "new"           => "sort by new",
    "price_down"    => "sort from high price",
    "price_up"      => "sort from low price",
);

$pagination = array(
    "first"         => "First",
    "last"          => "Last",
    "next"          => "next",
    "prev"          => "prev",
    "total"         => "total: 13 pages",
);

$site_cart = array(
    "del"           => "remove",
    "limited"       => "limit for purchase one item",
    "gift_text"     => "gift-product name",
    "add_text"      => "additional product",
    "buy-discount"  => "Discount",
    "tag1"          => "fit",
    "tag_text"      => "$50000立折$500",
    "combo1"        => "include below: ",
    "combo2"        => "combo A",
    "combo3"        => "combo B",
    "sale"          => "discount-(滿$2000折200)",
    "total"         => "total",
    "checkOut"      => "check out",
);

$L10n_item = array(
    "sale_title"     => "promotion period: ",
    "sale_price"     => "promotion price: ",
    "sale_count"     => "count down: ",
    "sale_people"    => " people bought",
    "desc"          =>  "description: ",
    "color"         => "color: ",
    "color_text"    => "color",
    "spec"          => "spec: ",
    "include"       => "include: ",
    "include_item"  => "tied product",
    "spec_text"     => "choose spec",
    "gift"          => "gift",
    "gift_text"     => "its gift",
    "gift_photo"    => "(photo)",
    "high_price"    => "suggest price: ",
    "price"         => "net price: ",
    "highlight1"    => "limit for purchasing (remained quantity: 9999)",
    "highlight2"    => "estimated time of delivery: 2014/05/16",
    "h3_add"        => "Additional:",
    "h3_intro"      => "Product Introduction",
);

$L10n_search = array(
    "placeholder"   => "search..",
);

$btn = array(
    "btn-buy"                   => "Buy",
    "btn-preorder"          => "PreOrder",
    "btn-preorder-finish"   => "Sold Out",
    "btn-rush"              => "Buy",
    "btn-rush-finish"       => "Sold Out",
    "btn-discount"          => "Discount",
    "btn-soldout-stock"     => "replenishing",
    "btn-soldout"           => "Sold Out",
    "btn-to-sale"           => "To Sale",
    "btn-select"            => "Choose size",
    "btn-addcart"           => "Add to Cart",
);

$rwd_index_001 = array(
    array(
        "name"      => "輪播圖1",
        "images"    => "../c/img/sth_htc/slide_1.jpg",
        "url"       => "item.php",
    ),
    array(
        "name"      => "輪播圖1",
        "images"    => "../c/img/sth_htc/slide_2.jpg",
        "url"       => "item.php",
    ),
);

$rwd_index_009_title = "What's New";

$itemss = array(
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_01.png",
        "images-full"    => "http://placehold.it/600x600",
        "url"       => "../front/item.php",
        "price"     => "4,299",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
        "gift"   => array(
            "薯條小兔公仔",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_02.png", 
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_03.png",
        "url"       => "../front/item.php",
        "price"     => "2,480",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_04.png",
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_05.png",
        "url"       => "../front/item.php",
        "price"     => "1,680",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_06.png",
        "url"       => "../front/item.php",
        "price"     => "1,980",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_07.png",
        "url"       => "../front/item.php",
        "price"     => "3,480",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_08.png",
        "url"       => "../front/item.php",
        "price"     => "3,480",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_09.png",
        "url"       => "../front/item.php",
        "price"     => "3,880",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_10.png",
        "url"       => "../front/item.php",
        "price"     => "3,680",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
);

$rwd_index_006_title = "Accessories";
$rwd_index_004_title = "Special Sales";

$rwd_index_009 = array(
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_01.png", 
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_02.png", 
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_03.png", 
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_04.png", 
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_05.png", 
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_06.png", 
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_07.png", 
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_08.png", 
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_09.png", 
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_10.png", 
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "The HTC One E8 smartphone is stylish and cool, the main body is crafted out of a smooth aluminium that holds the 5 Full HD 1920 x 1080 screen snugly to the edge of the phone. ",
        "feature"   => array(
            "5inch, Full HD 1080P",
            "Qualcomm® Snapdragon™ 801，Quad CoreCPU",
            "Max. slot capacity: 128 GB / Slot type: microSD / RAM: 2 GB / Internal: 16 GB ",
        ),
    ),
);
$rwd_index_006 = array(
    array(
        "name"      => "HTC One M8",
        "images"    => "../c/img/sth_htc/prod_11.png",
        "images-full"    => "http://placehold.it/600x600",
        "url"       => "../front/item.php",
        "price"     => "8,888",
        "stock"     => true,
        "type"      => "btn-preorder",
        "desc"      => "利用 Motion Launch™ 手勢啟動，便能快速進入應用程式和手機螢幕。只要滑動手指，您就能開啟 HTC BlinkFeed™ 首頁",
        "feature"   => array(
            "完美自拍，分享精彩",
            "超強電池續航力",
            "個人化動態首頁",
        ),
        "gift"   => array(
            "贈品1",
            "贈品2",
            "贈品3",
        ),
    ),
    array(
        "name"      => "HTC One Mini 2",
        "images"    => "../c/img/sth_htc/prod_12.png",
        "images-full"    => "http://placehold.it/600x600",
        "url"       => "../front/item.php",
        "price"     => "8,888",
        "stock"     => true,
        "type"      => "btn-preorder",
        "desc"      => "利用 Motion Launch™ 手勢啟動，便能快速進入應用程式和手機螢幕。只要滑動手指，您就能開啟 HTC BlinkFeed™ 首頁",
        "feature"   => array(
            "完美自拍，分享精彩",
            "超強電池續航力",
            "個人化動態首頁",
        ),
        "gift"   => array(
            "贈品1",
            "贈品2",
            "贈品3",
        ),
    ),
    array(
        "name"      => "HTC Desire 816",
        "images"    => "../c/img/sth_htc/prod_13.png",
        "images-full"    => "http://placehold.it/600x600",
        "url"       => "../front/item.php",
        "price"     => "8,888",
        "stock"     => true,
        "type"      => "btn-preorder",
        "desc"      => "利用 Motion Launch™ 手勢啟動，便能快速進入應用程式和手機螢幕。只要滑動手指，您就能開啟 HTC BlinkFeed™ 首頁",
        "feature"   => array(
            "完美自拍，分享精彩",
            "超強電池續航力",
            "個人化動態首頁",
        ),
        "gift"   => array(
            "贈品1",
            "贈品2",
            "贈品3",
        ),
    ),
    array(
        "name"      => "HTC Desire 816",
        "images"    => "../c/img/sth_htc/prod_14.png",
        "images-full"    => "http://placehold.it/600x600",
        "url"       => "../front/item.php",
        "price"     => "8,888",
        "stock"     => true,
        "type"      => "btn-preorder",
        "desc"      => "利用 Motion Launch™ 手勢啟動，便能快速進入應用程式和手機螢幕。只要滑動手指，您就能開啟 HTC BlinkFeed™ 首頁",
        "feature"   => array(
            "完美自拍，分享精彩",
            "超強電池續航力",
            "個人化動態首頁",
        ),
        "gift"   => array(
            "贈品1",
            "贈品2",
            "贈品3",
        ),
    ),
    array(
        "name"      => "HTC Desire 816",
        "images"    => "../c/img/sth_htc/prod_15.png",
        "images-full"    => "http://placehold.it/600x600",
        "url"       => "../front/item.php",
        "price"     => "8,888",
        "stock"     => true,
        "type"      => "btn-preorder",
        "desc"      => "利用 Motion Launch™ 手勢啟動，便能快速進入應用程式和手機螢幕。只要滑動手指，您就能開啟 HTC BlinkFeed™ 首頁",
        "feature"   => array(
            "完美自拍，分享精彩",
            "超強電池續航力",
            "個人化動態首頁",
        ),
        "gift"   => array(
            "贈品1",
            "贈品2",
            "贈品3",
        ),
    ),
);

$items_color = array(
    array(
        "name"      => "白色",
        "url"       => "../c/img/sth001/sample/color_01.jpg",
    ),
    array(
        "name"      => "粉紅色",
        "url"       => "../c/img/sth001/sample/color_02.jpg",
    ),
    array(
        "name"      => "黃色",
        "url"       => "../c/img/sth001/sample/color_03.jpg",
    ),
    array(
        "name"      => "紫色",
        "url"       => "../c/img/sth001/sample/color_04.jpg",
    ),
    array(
        "name"      => "媚綠色",
        "url"       => "../c/img/sth001/sample/color_05.jpg",
    ),
);

$cart_items = array(
    array(
        "name"      => "HTC ONE M8",
        "images"    => "http://placehold.it/60x60",
        "url"       => "../front/item.php",
        "price"     => "9999999.99",
        "type"      => "add",
    ),
    array(
        "name"      => "HTC ONE M8",
        "images"    => "http://placehold.it/60x60",
        "url"       => "../front/item.php",
        "price"     => "4,500",
        "type"      => "",
    ),
    array(
        "name"      => "HTC ONE combo",
        "images"    => "http://placehold.it/60x60",
        "url"       => "../front/item.php",
        "price"     => "4,500",
        "type"      => "combo",
    ),
    array(
        "name"      => "HTC ONE M8",
        "images"    => "http://placehold.it/60x60",
        "url"       => "../front/item.php",
        "price"     => "4,500",
        "type"      => "",
    ),
);

$recommends = array(
    "小鼠系列",
    "RockCOCO",
);
$logo = array(
    "images"    => "../c/img/sth_htc/logo.gif",
);
$category_items = array(
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_01.png",
        "images-full"    => "http://placehold.it/600x600",
        "url"       => "../front/item.php",
        "price"     => "4,299",
        "stock"     => true,
        "type"      => "btn-addcart",
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
        "gift"   => array(
            "薯條小兔公仔",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_02.png", 
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_03.png",
        "url"       => "../front/item.php",
        "price"     => "2,480",
        "stock"     => true,
        "type"      => "",
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_04.png",
        "url"       => "../front/item.php",
        "price"     => "1,280",
        "stock"     => true,
        "type"      => "",
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_05.png",
        "url"       => "../front/item.php",
        "price"     => "1,680",
        "stock"     => true,
        "type"      => "",
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_06.png",
        "url"       => "../front/item.php",
        "price"     => "1,980",
        "stock"     => true,
        "type"      => "",
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_07.png",
        "url"       => "../front/item.php",
        "price"     => "3,480",
        "stock"     => true,
        "type"      => "",
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_08.png",
        "url"       => "../front/item.php",
        "price"     => "3,480",
        "stock"     => true,
        "type"      => "",
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_09.png",
        "url"       => "../front/item.php",
        "price"     => "3,880",
        "stock"     => true,
        "type"      => "",
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
    array(
        "name"      => "HTC Desire 820",
        "images"    => "../c/img/sth_htc/prod_10.png",
        "url"       => "../front/item.php",
        "price"     => "3,680",
        "stock"     => true,
        "type"      => "",
        "desc"      => "5 吋斜角層疊式顯示畫面，給你清晰無邊界的視覺體驗。 精緻膠質機殼手感更輕盈，絕佳手感，經久耐用",
        "feature"   => array(
            "新渴望8系，荣耀回归",
            "震撼视界",
            "旗舰风尚 潮我看",
        ),
    ),
);
?>