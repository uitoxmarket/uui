<?php
    include "../frame/__config.php";
    include "../frame/__data.php";
    include "__data.php";
    $type="pc";
?>
<!doctype html>
<html lang="zh-TW">
<head>
    <?php meta();?>
</head>
<body>
    <div class="site-outter">
        <?php include "../module_component/_site_ad_top.php";?>
        <?php include "../module_header/_pc_header_001.php";?>
        <div class="site-body">
            <div class="groupbuy-top-banner"><img src="../c/img/frame/1200x200.jpg" alt=""/></div>
            <aside class="site-left">
                <?php include "../module_left/_pc_left_001.php";?>
            </aside>
            <article class="site-main">
                <div class="nav-breadcrumbs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <div class="container">
                        <div class="cate" itemprop="child" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="index.php" itemprop="url">
                                <span itemprop="title">首頁</span>
                            </a>
                            &gt; 
                            <span itemprop="title" class="bread-item-name">限時特賣</span>
                        </div>
                    </div>
                </div>
                <div class="site-sort">
                    <div class="container">
                        <ul class="sort">
                            <li><a href="#" class="active">價差最大</a></li>
                            <li><a href="#">即將結束</a></li>
                            <li><a href="#">熱賣暢銷</a></li>
                            <li><a href="#">最新上架</a></li>
                        </ul>
                    </div>
                </div>
                
                <?php include "../module_market/_pc_market_001.php";?>
                <div class="groupbuy-04">
                    <div class="container">
                        <div class="items">
                            <div itemscope="" itemtype="http://data-vocabulary.org/Product" class="item">
                                <h1 itemprop="name" class="name"><a href="#">【PUFII】秋季熱銷品 自訂款素面修身襯衫</a></h1>
                                <a href="item.php" itemprop="offerurl" class="photo"><img src="../c/img/sth_asap/sample/item_10.jpg" alt=""><div class="discount_tip"></div></a>
                                <div class="info">
                                    <div class="time"><b>特賣結束時間：</b>23:59:59</div>
                                    <div class="feature">
                                            <ul>
                                                <li> 新渴望8系，荣耀回归 </li>
                                                <li> 震撼视界</li>
                                                <li> 旗舰风尚 潮我看 </li>
                                                <li> 旗舰风尚 潮我看 </li>
                                            </ul>
                                    </div>
                                    <div class="buy is-old">
                                        <span class="currency">NT$</span>
                                        <span itemprop="price" class="price">459999</span>
                                    </div>
                                    <div class="buy">
                                        <span class="currency">NT$</span>
                                        <span itemprop="price" class="price">3,680999</span>
                                        <span class="discount">(價差：$9999999)</span>
                                        <a href="item.php" class="btn" itemprop="offerurl">立即搶購<i class="fa fa-chevron-circle-right"></i></a>
                                    </div>
                                    <div class="buy">
                                        <span class="discount-sale-num">販售數量：123456789</span>
                                    </div>
                                </div>
                            </div>

                            <div itemscope="" itemtype="http://data-vocabulary.org/Product" class="item">
                                <h1 itemprop="name" class="name"><a href="#">【PUFII】秋季熱銷品 自訂款素面修身襯衫</a></h1>
                                <a href="item.php" itemprop="offerurl" class="photo"><img src="../c/img/sth_asap/sample/item_09.jpg" alt=""><div class="discount_tip is-txt"></div></a>
                                <div class="info">
                                    <div class="time is-come-end"><b>特賣即將結束</b></div>
                                    <div class="feature">
                                            <ul>
                                                <li> 新渴望8系，荣耀回归 </li>
                                                <li> 震撼视界</li>
                                                <li> 旗舰风尚 潮我看 </li>
                                                <li> 旗舰风尚 潮我看 </li>
                                            </ul>
                                    </div>
                                    <div class="buy is-old">
                                        <span class="currency">NT$</span>
                                        <span itemprop="price" class="price">459999</span>
                                    </div>
                                    <div class="buy">
                                        <span class="currency">NT$</span>
                                        <span itemprop="price" class="price">3,680</span>
                                        <span class="discount">(價差：$20000)</span>
                                        <a href="item.php" class="btn" itemprop="offerurl">立即搶購<i class="fa fa-chevron-circle-right"></i></a>
                                    </div>
                                    <div class="buy">
                                        <span class="discount-sale-num">販售數量：123456789</span>
                                    </div>
                                </div>
                            </div>

                            <div itemscope="" itemtype="http://data-vocabulary.org/Product" class="item">
                                <h1 itemprop="name" class="name"><a href="#">【PUFII】秋季熱銷品 自訂款素面修身襯衫</a></h1>
                                <a href="item.php" itemprop="offerurl" class="photo"><img src="../c/img/sth_asap/sample/item_07.jpg" alt=""><div class="discount_tip"></div></a>
                                <div class="info">
                                    <div class="time"><b>特賣結束時間：</b>23:59:59</div>
                                    <div class="feature">
                                            <ul>
                                                <li> 新渴望8系，荣耀回归 </li>
                                                <li> 震撼视界</li>
                                                <li> 旗舰风尚 潮我看 </li>
                                                <li> 旗舰风尚 潮我看 </li>
                                            </ul>
                                    </div>
                                    <div class="buy is-old">
                                        <span class="currency">NT$</span>
                                        <span itemprop="price" class="price">459999</span>
                                    </div>
                                    <div class="buy">
                                        <span class="currency">NT$</span>
                                        <span itemprop="price" class="price">3,680</span>
                                        <span class="discount">(價差：$20000)</span>
                                        <a href="item.php" class="btn" itemprop="offerurl">立即搶購<i class="fa fa-chevron-circle-right"></i></a>
                                    </div>
                                    <div class="buy">
                                        <span class="discount-sale-num">販售數量：123456789</span>
                                    </div>
                                </div>
                            </div>

                            <div itemscope="" itemtype="http://data-vocabulary.org/Product" class="item">
                                <h1 itemprop="name" class="name"><a href="#">【PUFII】秋季熱銷品 自訂款素面修身襯衫</a></h1>
                                <a href="item.php" itemprop="offerurl" class="photo"><img src="../c/img/sth_asap/sample/item_01.jpg" alt=""><div class="discount_tip"></div></a>
                                <div class="info">
                                    <div class="time is-gone"><b>特賣結束</b></div>
                                    <div class="feature">
                                            <ul>
                                                <li> 新渴望8系，荣耀回归 </li>
                                                <li> 震撼视界</li>
                                                <li> 旗舰风尚 潮我看 </li>
                                                <li> 旗舰风尚 潮我看 </li>
                                            </ul>
                                    </div>
                                    <div class="buy is-old">
                                        <span class="currency">NT$</span>
                                        <span itemprop="price" class="price">459999</span>
                                    </div>
                                    <div class="buy">
                                        <span class="currency">NT$</span>
                                        <span itemprop="price" class="price">3,680</span>
                                        <span class="discount">(價差：$20000)</span>
                                        <a href="item.php" class="btn" itemprop="offerurl">立即搶購<i class="fa fa-chevron-circle-right"></i></a>
                                    </div>
                                    <div class="buy">
                                        <span class="discount-sale-num">販售數量：123456789</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pc_market_list">
                    <div class="container">
                        <div class="items">
                            <?php for ($x = 0; $x <= 19; $x++) { 
                            echo '
                                <div itemscope="" itemtype="http://data-vocabulary.org/Product" class="item">
                                    <a href="item.php" itemprop="offerurl" class="photo">
                                            <img src="../c/img/sth_asap/sample/item_05.jpg" alt="">
                                            <div class="discount_tip"></div>
                                    </a>
                                    <div class="info">
                                        <h1 itemprop="name" class="name"><a href="#">【PUFII】秋季熱銷品 自訂款素面修身襯衫</a></h1>
                                        <div class="buy is-old">
                                            <span class="currency">NT$</span>
                                            <span itemprop="price" class="price">3,680</span>
                                        </div>
                                        <div class="buy">
                                            <span class="currency">價差↓</span>
                                            <span itemprop="price" class="price"> $5999</span>
                                            <a href="item.php" class="btn" itemprop="offerurl">搶購<i class="fa fa-chevron-circle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            ';}?>
                        </div>
                    </div>
                </div>
                <?php include "../module_component/_pc_nav_pagination.php";?>
            </article>
            <!-- <aside class="site-right">
                右欄預留空間
            </aside> -->
        </div>
        <div class="nav-to-top">
            <i class="fa fa-angle-double-up"></i>
        </div>
    </div>
    <?php include "../frame/__pc_js_library.php";?>
</body>
</html>