<div class="pc_left_002">
    <h3 class="title-h3 title-h3-active"><a href="#">身體保養</a></h3>
    <ul class="side-menu">
        <li>
            <h4><i class="fa fa-minus-square-o"></i>防曬系列</h4>
            <ul class="sec-menu hide">
                <!-- <li><a href="category.php" class="active">新品上架新品上架新品限制最多顯示兩行</a></li> -->
                <li><a href="category.php">臉部防曬／隔離霜</a></li>
                <li><a href="category.php">身體防曬</a></li>
                <li><a href="category.php">美體保養</a></li>
                <li><a href="category.php">助曬劑</a></li>
            </ul>
        </li>
        <li>
            <h4><i class="fa fa-plus-square-o"></i>美體乳液/霜</h4>
            <ul class="sec-menu hide">
                <li><a href="category.php">纖體乳液</a></li>
                <li><a href="category.php">私密清潔／護理</a></li>
            </ul>
        </li>
        <li>
            <h4><i class="fa fa-plus-square-o"></i>手部保養</h4>
            <ul class="sec-menu hide">
                <li><a href="category.php">護手霜</a></li>
                <li><a href="category.php">指緣油</a></li>
                <li><a href="category.php">手膜</a></li>
            </ul>
        </li>
        <li>
            <h4><i class="fa fa-plus-square-o"></i>清潔沐浴</h4>
            <ul class="sec-menu hide">
                <li><a href="category.php">沐浴乳</a></li>
                <li><a href="category.php">沐浴皂</a></li>
                <li><a href="category.php">潤膚</a></li>
                <li><a href="category.php">寶寶沐浴用品</a></li>
                <li><a href="category.php">去角質</a></li>
            </ul>
        </li>
        <li>
            <h4><i class="fa fa-plus-square-o"></i>清潔沐浴小物</h4>
            <ul class="sec-menu hide">
                <li><a href="category.php">沐浴海綿</a></li>
                <li><a href="category.php">木柄刷</a></li>
                <li><a href="category.php">浮石</a></li>
                <li><a href="category.php">沐浴巾</a></li>
            </ul>
        </li>
        <li>
            <h4><i class="fa fa-plus-square-o"></i>口腔清潔</h4>
            <ul class="sec-menu hide">
                <li><a href="category.php">牙刷</a></li>
                <li><a href="category.php">牙膏／牙粉</a></li>
                <li><a href="category.php">牙線</a></li>
                <li><a href="category.php">寶寶用潔牙紗布</a></li>
                <li><a href="category.php">漱口水</a></li>
                <li><a href="category.php">假牙清潔</a></li>
            </ul>
        </li>
        <li>
            <h4><i class="fa fa-plus-square-o"></i>美髮護理</h4>
            <ul class="sec-menu hide">
                <li><a href="category.php">洗鬖</a></li>
                <li><a href="category.php">潤髮</a></li>
                <li><a href="category.php">護髮</a></li>
                <li><a href="category.php">造型</a></li>
                <li><a href="category.php">按摩保養</a></li>
                <li><a href="category.php">精油</a></li>
                <li><a href="category.php">按摩油</a></li>
                <li><a href="category.php">按摩板</a></li>
                <li><a href="category.php">水氧機</a></li>
            </ul>
        </li>
        <li>
            <h4><i class="fa fa-plus-square-o"></i>孕婦保養</h4>
            <ul class="sec-menu hide">
                <li><a href="category.php">除紋按摩乳液</a></li>
                <li><a href="category.php">美胸霜</a></li>
                <li><a href="category.php">撫紋乳</a></li>
                <li><a href="category.php">潔顏乳</a></li>
                <li><a href="category.php">孕婦專用指甲油</a></li>
            </ul>
        </li>
    </ul>
    <!-- <h3 class="title-h3"><a href="#">電腦、周邊、軟體</a></h3>
    <h3 class="title-h3"><a href="#">手機、配件</a></h3>    
    <h3 class="title-h3"><a href="#">相機、攝影</a></h3>
    <h3 class="title-h3"><a href="#">流行服飾、內睡衣</a></h3>
    <h3 class="title-h3"><a href="#">包包、精品、飾品</a></h3> -->
</div>