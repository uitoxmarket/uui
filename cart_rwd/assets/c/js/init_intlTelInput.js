//init intlTelInput
(function($){
    $('.input-intl-phone').intlTelInput({
        defaultCountry: "auto",
        utilsScript: "assets/c/js/vendor/intlTelInput-utils.js"
    });
})(jQuery);